<?php
include "../../clases/database.php";
include "../../clases/archivo.php";

if(isset($_FILES["archivoExcel"])){

    $idSucursal =0;
    if(isset($_POST['idSucursal'])){
        $idSucursal =$_POST['idSucursal'];
    }

    $objArchivo = new archivo();    
    $archivo = "./uploads/".$_FILES["archivoExcel"]["name"];
    $objArchivo->ruta_temporal=$_FILES["archivoExcel"]["tmp_name"];
    $objArchivo->ruta_final=$archivo;
    $objArchivo->Subir();

    if($_FILES["archivoExcel"]["name"] != "" && file_exists($archivo))
    {
        /// leer el archivo excel
        require_once '../../PHPExcel/Classes/PHPExcel.php';        
        $inputFileType = PHPExcel_IOFactory::identify($archivo);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($archivo);
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();
        for ($row = 2; $row <= $highestRow; $row++){ 
            $x_sucursal = $sheet->getCell("A".$row)->getValue();
            $x_ncot1 = $sheet->getCell("B".$row)->getValue();
            $x_remision = $sheet->getCell("C".$row)->getValue();
            $x_fremision = $sheet->getCell("D".$row)->getValue();
            $x_empresa = $sheet->getCell("E".$row)->getValue();
            $x_claveprod = $sheet->getCell("F".$row)->getValue();
            $x_descprod = $sheet->getCell("G".$row)->getValue();
            $x_cantprod = $sheet->getCell("H".$row)->getValue();
            $x_precpublico = $sheet->getCell("I".$row)->getValue();
            $x_ivaprod = $sheet->getCell("J".$row)->getValue();
            $x_totalprod = $sheet->getCell("K".$row)->getValue();
            $x_totalfinal = $sheet->getCell("L".$row)->getValue();
            $x_totalfinal = number_format($x_totalfinal, 2, '.', ',');
            $x_atendio = $sheet->getCell("M".$row)->getValue();
            $x_impresion = $sheet->getCell("N".$row)->getValue();

            $sql = "insert into matriz (sucursal, ncot1, remision, fremision, empresa, claveprod, descprod, cantprod, precpublico, ivaprod, totalprod, totalfinal, atendio, impresion, idSucursal) value ";
            $sql .= " (\"$x_sucursal\",\"$x_ncot1\",\"$x_remision\",\"$x_fremision\",\"$x_empresa\",\"$x_claveprod\",\"$x_descprod\", \"$x_cantprod\",\"$x_precpublico\",\"$x_ivaprod\",\"$x_totalprod\",\"$x_totalfinal\", \"$x_atendio\", \"$x_impresion\", \"$idSucursal\")";
            $con->query($sql);
        }
    	unlink($archivo);//Eliminar archivo
    }
}

header('location: ver.php?idSucursal='.$idSucursal);
?>