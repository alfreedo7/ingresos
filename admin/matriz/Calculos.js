

function CalcularTotal() {
    let total = 0;
    let subTotal = 0;
    let precio = 0;
    let iva = 0;
    let cantidad = 0;

    cantidad = $('#txtCantidad').val();
    precio = $('#txtPrecio').val();

    if (precio === undefined || precio == '')
        precio = 0;

    if (cantidad === undefined || cantidad == '')
        cantidad = 0;

    subTotal = roundTo(parseFloat(cantidad) * parseFloat(precio), 2);

    iva = roundTo(subTotal * 0.16, 2);
    total = roundTo(subTotal + iva, 2);
    
    $('#txtIva').val(iva);
    $('#txtSubTotal').val(subTotal);
    $('#txtTotal').val(total);
}

function roundTo(numero, decimales) {
    numeroRegexp = new RegExp('\\d\\.(\\d){' + decimales + ',}');   // Expresion regular para numeros con un cierto numero de decimales o mas
    if (numeroRegexp.test(numero)) {         // Ya que el numero tiene el numero de decimales requeridos o mas, se realiza el redondeo
        return Number(numero.toFixed(decimales));
    } else {
        return Number(numero.toFixed(decimales)) === 0 ? 0 : numero;  // En valores muy bajos, se comprueba si el numero es 0 (con el redondeo deseado), si no lo es se devuelve el numero otra vez.
    }
}

$(document).ready(function () {
    var txtPrecio = document.getElementById("txtPrecio");
    txtPrecio.addEventListener("input", CalcularTotal, false);

    var txtCantidad = document.getElementById("txtCantidad");
    txtCantidad.addEventListener("input", CalcularTotal, false);


});    