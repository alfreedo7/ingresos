<?php 
include('../../clases/matriz.php');

$matriz=new matriz();

$operacion ="";
if(isset($_GET['operacion'])){
	$operacion =$_GET['operacion'];
	$matriz->idSucursal=$_GET['idSucursal'];
}else{
	if(isset($_POST['operacion'])){
		$operacion=$_POST['operacion'];

		$matriz->idMatriz=$_POST['idMatriz'];
		$matriz->sucursal=$_POST['sucursal'];
		$matriz->ncot1=$_POST['ncot1'];
		$matriz->remision=$_POST['remision'];
		$matriz->fremision=$_POST['fremision'];
		$matriz->empresa=$_POST['empresa'];
		$matriz->claveprod=$_POST['claveprod'];
		$matriz->descprod=$_POST['descprod'];
		$matriz->cantprod=$_POST['cantprod'];
		$matriz->precpublico=$_POST['precpublico'];
		$matriz->ivaprod=$_POST['ivaprod'];
		$matriz->totalprod=$_POST['totalprod'];
		$matriz->totalfinal=number_format($_POST['totalfinal'], 2, '.', ',');
		$matriz->atendio=$_POST['atendio'];
		$matriz->impresion=$_POST['impresion'];
		$matriz->idSucursal=$_POST['idSucursal'];
	}	
}

switch ($operacion) {
	case 'Agregar':
		# code...			
		$matriz->insertar_matriz();
		break;
	case 'Modificar':
		# code...
		$matriz->modificar_matriz();
		break;
	case 'Eliminar':
		# code...        
		$matriz->eliminar_matriz();
		break;
	case 'Reiniciar':
		# code...
		$matriz->Reiniciar_tabla_matriz();
		break;
	default:
		# code...
		break;
}
header('location: ver.php?idSucursal='.$matriz->idSucursal);

//print_r($matriz);
?>