<?php
session_start();
include_once('../../clases/seguridad.php');
$seg=new seguridad();
$seg->candado('../../login.php');
$seg->candado_permiso($_SESSION['idUsuario'], 'SUPADMIN');

include_once('../../clases/matriz.php');

include_once('../../clases/usuario.php');
$ObjUser=new usuario();
$ObjUser->idUsuario=$_SESSION['idUsuario'];
$ObjUser->obtener_usuario();


$matriz=new matriz();

if(isset($_GET['idSucursal'])){
	$matriz->idSucursal =$_GET['idSucursal'];
  // $lista=$matriz->listar_orden_matriz();
  $lista=$matriz->listar_matriz_sucursal();
}

?>



<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="shortcut icon" type="image/x-icon" href="../../img/icono.ico">

  <title>Ver registros Sucursales</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php include_once ('../elementos/sidebar-admin.php'); ?>    

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->

        <?php include_once ('../elementos/topbar-admin.php');  ?>
        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">


          <div class="d-sm-flex align-items-center justify-content-between mb-4">

            <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-file-excel fa-sm text-white-50"></i> Importar Excel</button>

            <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" data-toggle="modal" data-target="#modalConfirmar">
              <i class="fas fa-download fa-sm text-white-50"></i> Vaciar Listado
            </button>
            <!-- <a href="operaciones_matriz.php?operacion=Reiniciar" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Vaciar Listado</a> -->

            <a href="../../descarga/estructura.xlsx" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Descargar Plantilla Excel</a>           

          </div>



          <!-- Page Heading -->
           <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Registros</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      
                      <th>Sucursal</th>
                      <th>N.Cot</th>
                      <th>Remision</th>
                      <th>Fecha</th>
                      <th>Empresa</th>
                      <th>Clave</th>
                      <th>Descripcion</th>
                      <th>Cant.</th>
                      <th>Precio</th>
                      <th>Iva</th>
                      <th>SubTotal</th>
                      <th>Total</th>
                      <th>Editar</th>
                      <th>Imprimir</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php 
            foreach ($lista as $elemento) {
                echo '<tr> 

                <td>'.$elemento['sucursal'].'</td>
                <td>'.$elemento['ncot1'].'</td>
                <td>'.$elemento['remision'].'</td>
                <td>'.$elemento['fremision'].'</td>
                <td>'.$elemento['empresa'].'</td>
                <td>'.$elemento['claveprod'].'</td>
                <td>'.$elemento['descprod'].'</td>
                <td>'.$elemento['cantprod'].'</td>
                <td>'.$elemento['precpublico'].'</td>
                <td>'.$elemento['ivaprod'].'</td>
                <td>'.$elemento['totalprod'].'</td>
                <td>'.$elemento['totalfinal'].'</td>
                <td><a href="edit.php?id='.$elemento['idMatriz'].'"><img src="../../img/svg/edit-2.svg"></span></a></td>
                <td><a onclick="imprimirTicket('.$elemento['idMatriz'].','.$elemento['idSucursal'].');" style="cursor:pointer"><img src="../../img/svg/printer.svg"></span></a></td>
                
                </tr>';
            }
            ?>
                   
                   
                  </tbody>
                </table>
              </div>
            </div>
          </div>
           


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <form method="post" id="addproduct" action="import.php" enctype="multipart/form-data" role="form">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Importar Datos Mediante Un Excel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">            
                <div>
                  <label class="control-label">Archivo (.xlsx)*</label>
                  <input type="file" name="archivoExcel"  id="name" onchange="return validarExt()" placeholder="Archivo (.xlsx)">
                  <input type="hidden" name="idSucursal" value="<?php echo $matriz->idSucursal;?>"/>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success">Importar Datos</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <div class="modal fade" id="modalConfirmar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">          
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Confirmar</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div>
                <label class="control-label">¿Deseas eliminar todos los registros?</label>                
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                Cancelar
              </button>
              <?php echo '<a href="operaciones_matriz.php?operacion=Reiniciar&idSucursal='.$matriz->idSucursal.'" class="btn btn-primary">Aceptar</a>' ?>
            </div>
          </div>        
        </div>
      </div>




      <!-- Footer -->

      <?php include_once ('../elementos/footer-admin.php');   ?>
     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../../vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/chart-area-demo.js"></script>
  <script src="../../js/demo/chart-pie-demo.js"></script>

   <!-- Page level plugins -->
  <script src="../../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/datatables-demo.js"></script>

  <script type="text/javascript">

function validarExt()
{
    var name = document.getElementById('name');
    var archivoRuta = name.value;
    var extPermitidas = /(.xlsx)$/i;
    if(!extPermitidas.exec(archivoRuta)){
        alert('Asegurese de haber seleccionado un archivo .xlsx)');
        name.value = '';
        return false;
    }

    else
    {
        //PRevio del PDF
        if (name.files && name.files[0]) 
        {
            var visor = new FileReader();
            visor.onload = function(e) 
            {
                document.getElementById('visorArchivo').innerHTML = 
                '<embed src="'+e.target.result+'" width="500" height="375" />';
            };
            visor.readAsDataURL(name.files[0]);
        }
    }
}
function imprimirTicket(ticket, idSucursal){
  var iframe = document.createElement('iframe');
  iframe.src = "impresion.php?idMatriz=" + ticket +"&idSucursal="+idSucursal;
  document.body.appendChild(iframe).style.display="none";
  iframe.contentWindow.document.focus();
  iframe.contentWindow.document.print();
  iframe.contentWindow.document.close();  
}
</script>



</body>

</html>
