<?php
session_start();
include_once('../../clases/seguridad.php');
$seg=new seguridad();
$seg->candado("../../login.php");
$seg->candado_permiso($_SESSION['idUsuario'], 'SUPADMIN');

include_once('../../clases/matriz.php');

$matriz=new matriz();

include_once('../../clases/usuario.php');
$ObjUser=new usuario();
$ObjUser->idUsuario=$_SESSION['idUsuario'];
$ObjUser->obtener_usuario();



if(isset($_GET['id'])){
$operacion='Modificar';
$eliminar='<input  name="operacion" type="submit" class="btn btn-danger" value="Eliminar"/>';
$matriz->idMatriz=$_GET['id'];
$matriz->obtener_matriz();
}
else{
$operacion='Agregar';
$eliminar='';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="shortcut icon" type="image/x-icon" href="../../img/icono.ico">

  <title>Agregar Registro Suc. Matriz</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php include_once ('../elementos/sidebar-admin.php'); ?>
    

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->

        <?php include_once ('../elementos/topbar-admin.php');  ?>
        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
           

           

           

          <!-- Content Row -->
          <div class="row">

            

            <div class="col-lg-12 mb-4">

              <!-- Illustrations -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Agregar Registro Sucursal Matriz</h6>
                </div>
                <div class="card-body">
                  <div class="text-center">

              <form action="operaciones_matriz.php" method="POST">

                <input type="hidden" value="<?php echo $matriz->idMatriz;?>" name="idMatriz" class="form-control">
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="inputEmail4">Nombre Sucursal</label>
                        <input type="text" value="<?php echo $matriz->sucursal;?>" name="sucursal" class="form-control" required readonly />
                      </div>
                      <div class="form-group col-md-2">
                        <label for="inputEmail4">Numero cot</label>
                        <input type="text" value="<?php echo $matriz->ncot1;?>" name="ncot1" class="form-control" required readonly />
                      </div>
                      <div class="form-group col-md-2">
                        <label for="inputEmail4">Remision</label>
                        <input type="text" value="<?php echo $matriz->remision;?>" name="remision" class="form-control" required readonly />
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputEmail4">Fecha</label>
                        <input type="date" value="<?php echo $matriz->fremision;?>" name="fremision" class="form-control" required />
                      </div>
                    </div>



                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="inputEmail4">Empresa</label>
                        <input type="text" value="<?php echo $matriz->empresa;?>" name="empresa" class="form-control" required  readonly/> 
                      </div>
                      <div class="form-group col-md-3">
                        <label for="inputEmail4">Clave Prod.</label>
                        <input type="text" value="<?php echo $matriz->claveprod;?>" name="claveprod" class="form-control" required readonly />
                      </div>
                      <div class="form-group col-md-5">
                        <label for="inputEmail4">Descripcion</label>
                        <input type="text" value="<?php echo $matriz->descprod;?>" name="descprod" class="form-control" required readonly />
                      </div>
                    </div>



                    <div class="form-row">
                      <div class="form-group col-md-2">
                        <label for="inputEmail4">Cantidad</label>
                        <input id="txtCantidad" type="number" step="any" value="<?php echo $matriz->cantprod;?>" name="cantprod" class="form-control" required /> 
                      </div>
                      <div class="form-group col-md-3">
                        <label for="inputEmail4">Precio</label>
                        <input id="txtPrecio" type="number" step="any" value="<?php echo $matriz->precpublico;?>" name="precpublico" class="form-control" required />
                      </div>
                      <div class="form-group col-md-3">
                        <label for="inputEmail4">IVA</label>
                        <input id="txtIva" type="number" step="any" value="<?php echo $matriz->ivaprod;?>" name="ivaprod" class="form-control" required />
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputEmail4">SubTotal</label>
                        <input id="txtSubTotal" type="number" step="any" value="<?php echo $matriz->totalprod;?>" name="totalprod" class="form-control" required /> 
                      </div>
                    </div>



                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="inputEmail4">Total</label>
                        <input id="txtTotal" type="number" step="any" value="<?php echo $matriz->totalfinal;?>" name="totalfinal" class="form-control" required />
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputEmail4">Atendio</label>
                        <input type="text" value="<?php echo $matriz->atendio;?>" name="atendio" class="form-control"  />
                      </div>
                      <div class="form-group col-md-4">
                       <label for="inputEmail4">Impresion</label>
                        <input type="text" value="<?php echo $matriz->impresion;?>" name="impresion" class="form-control"  /> 
                      </div>

                      <input type="hidden" value="<?php echo $matriz->idSucursal;?>" name="idSucursal" class="form-control"  />
                    </div>                                        
                    
                   <br> 

                    <div class="form-row">
                      <div class="form-group col-md-3">
                        <button type="submit" class="btn btn-primary" name="operacion" onclick="return confirmar()" value="<?php echo "$operacion";?>">Modificar</button> &nbsp;
                        <?php echo $eliminar;?>
                                                                 
                      </div>
                      <div class="form-group col-md-2">
                         <button type="submit" class="btn btn-light" name="operacion" onclick="return confirmar()" value="<?php echo "$operacion";?>">Cancelar</button>  
                      </div>
                      <div class="form-group col-md-7">

                        
                      </div>
                    </div>
              </form>

                  </div> 
                   
                </div>
              </div>

              

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->

      <?php include_once ('../elementos/footer-admin.php');   ?>
     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../../vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/chart-area-demo.js"></script>
  <script src="../../js/demo/chart-pie-demo.js"></script>

  <script src="./Calculos.js"></script>

</body>

</html>
