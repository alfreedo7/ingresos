<?php
session_start();
include_once('../../clases/seguridad.php');
$seg=new seguridad();
$seg->candado();
$seg->candado_permiso($_SESSION['idUsuario'], 'SUPADMIN');

include_once('../../clases/matriz.php');

include_once('../../clases/usuario.php');
$ObjUser=new usuario();
$ObjUser->idUsuario=$_SESSION['idUsuario'];
$ObjUser->obtener_usuario();


$matriz=new matriz();
$lista=$matriz->listar_matriz();
?>



<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="shortcut icon" type="image/x-icon" href="../../img/icono.ico">

  <title>Ver registros Suc. Temozon</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php include_once ('../elementos/sidebar-admin.php'); ?>
    

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->

        <?php include_once ('../elementos/topbar-admin.php');  ?>
        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            
            <a href="../../descarga/estructura.xlsx" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Descargar Plantilla Excel</a> 
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Vaciar Listado</a>  
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Importar Excel</a>  
          </div>

        
           
           
          


          <!-- Page Heading -->
           <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Registros Suc. Temozon</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      
                      <th>Sucursal</th>
                      <th>N.Cot</th>
                      <th>Remision</th>
                      <th>Fecha</th>
                      <th>Empresa</th>
                      <th>Clave</th>
                      <th>Descripcion</th>
                      <th>Cant.</th>
                      <th>Precio</th>
                      <th>Iva</th>
                      <th>SubTotal</th>
                      <th>Total</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php 
            foreach ($lista as $elemento) {
                echo '<tr> 

                <td>'.$elemento['sucursal'].'</td>
                <td>'.$elemento['ncot1'].'</td>
                <td>'.$elemento['remision'].'</td>
                <td>'.$elemento['fremision'].'</td>
                <td>'.$elemento['empresa'].'</td>
                <td>'.$elemento['claveprod'].'</td>
                <td>'.$elemento['descprod'].'</td>
                <td>'.$elemento['cantprod'].'</td>
                <td>'.$elemento['precpublico'].'</td>
                <td>'.$elemento['ivaprod'].'</td>
                <td>'.$elemento['totalprod'].'</td>
                <td>'.$elemento['totalfinal'].'</td>
                <td><a href="edit.php?id='.$elemento['idMatriz'].'"><img src="../../img/svg/edit-2.svg"></span></a></td>
                
                </tr>';
            }
            ?>
                   
                   
                  </tbody>
                </table>
              </div>
            </div>
          </div>
           


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->

      <?php include_once ('../elementos/footer-admin.php');   ?>
     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../../vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/chart-area-demo.js"></script>
  <script src="../../js/demo/chart-pie-demo.js"></script>

   <!-- Page level plugins -->
  <script src="../../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/datatables-demo.js"></script>

</body>

</html>
