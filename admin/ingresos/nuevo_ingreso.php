<?php
session_start();

include_once('../../clases/seguridad.php');
$seg=new seguridad();
$seg->candado("../../login.php");
$seg->candado_permiso($_SESSION['idUsuario'], 'SUPADMIN');

if(isset($_POST['logOut'])){
    $seg->cerrar_sesion("../../login.php");
}


include_once('../../clases/usuario.php');
$ObjUser=new usuario();
$ObjUser->idUsuario=$_SESSION['idUsuario'];
$ObjUser->obtener_usuario();

include_once('../../clases/ingreso.php');
include_once('../../clases/alumno.php');
include_once('../../clases/unidad.php');
include_once('../../clases/clases.php');
include_once('../../clases/tipo.php');
include_once('../../clases/descuento.php');

$alumno=new alumno();
$lista_alumno=$alumno->listar_alumno();
$unidad=new unidad();
$lista_unidad=$unidad->listar_unidad();
$clases=new clases();
$lista_clases=$clases->listar_clases();
$tipo= new tipo();
$lista_tipo=$tipo->listar_tipo();
$descuento= new descuento();
$lista_descuento=$descuento->listar_descuento();

$ingreso=new ingreso();
if(isset($_GET['id']))
{
    $operacion='Modificar';
    $ingreso->idIngreso=$_GET['id'];
    $ingreso->obtener_ingreso();
    
    $boton_eliminar='<input type="submit"  name="operacion" class="btn btn-primary" value="Eliminar" >';
}else{
    $operacion='Agregar';
    $boton_eliminar='';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="shortcut icon" type="image/x-icon" href="../../img/idey.ico">

  <title>Nuevo Ingreso || Ingresos</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php include_once ('../elementos/sidebar-admin.php'); ?>
    

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->

        <?php include_once ('../elementos/topbar-admin.php');  ?>
        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
           

           

           

          <!-- Content Row -->
          <div class="row">

            

            <div class="col-lg-12 mb-4">

              <!-- Illustrations -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Agregar Nuevo Ingreso</h6>
                </div>
                <div class="card-body">
                  <div class="text-center">

              <form action="operaciones_ingreso.php" method="POST">

                <input type="hidden" value="<?php echo $ingreso->idIngreso;?>" name="idIngreso" class="form-control">
                    
                    <div class="form-row">
                      
                      <div class="form-group col-md-5">
                        <label for="inputEmail4">Nombre del Alumno</label>
                        <div class="form-select-list">
                            <select class="form-control custom-select-value" name="idAlumno">
                                <?php 
                                      foreach ($lista_alumno as $elemento) {
                                        $select='';
                                        if ($elemento['idAlumno']==$ingreso->idAlumno) 
                                          $select=' selected';
                                        echo '<option '.$select.' value="'.$elemento['idAlumno'].'">'.$elemento['nomalumno'].'</option>';
                                      }
                                  ?>
                                                                    
                            </select>
                        </div>
                      </div>
                      
                      <div class="form-group col-md-3">
                        <label for="inputPassword4">Unidad Deportiva</label>
                        <div class="form-select-list">
                          <select class="form-control custom-select-value" name="idUnidad">
                              <?php 
                                      foreach ($lista_unidad as $elemento) {
                                        $select='';
                                        if ($elemento['idUnidad']==$ingreso->idUnidad) 
                                          $select=' selected';
                                        echo '<option '.$select.' value="'.$elemento['idUnidad'].'">'.$elemento['nombre_unidad'].'</option>';
                                      }
                                  ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group col-md-4">
                        <label for="inputEmail4">Clase</label>
                        <div class="form-select-list">
                           <select class="form-control custom-select-value" name="idClase">
                                  <?php 
                                      foreach ($lista_clases as $elemento) {
                                        $select='';
                                        if ($elemento['idClase']==$ingreso->idClase) 
                                          $select=' selected';
                                        echo '<option '.$select.' value="'.$elemento['idClase'].'">'.$elemento['nombre_clase'].'</option>';
                                      }
                                  ?>
                            </select>
                        </div>
                      </div>

                    </div>

                      

                    <div class="form-row">

                      <div class="form-group col-md-2">
                        <label for="inputPassword4">Tipo de pago</label>
                          <div class="form-select-list">
                            <select class="form-control custom-select-value" onchange="select_tipo();" name="idTipo" id="idTipo" require="">
                              <?php 
                                        foreach ($lista_tipo as $elemento) {
                                          $select='';
                                          if ($elemento['idTipo']==$ingreso->idTipo) 
                                            $select=' selected';
                                          echo '<option '.$select.' value="'.$elemento['idTipo'].'">'.$elemento['nombre'].'</option>';
                                        }
                              ?>
                            </select>
                          </div>
                      </div>

                      <div class="form-group col-md-3">
                        <label for="inputPassword4">Mes</label>
                          <div class="form-select-list">
                            <select class="form-control custom-select-value" name="mes" id="mes">
                            <option value="<?php echo $ingreso->mes;?>"><?php echo $ingreso->mes;?></option>
                                                      <option value="Enero">Enero</option>
                                                      <option value="Febrero">Febrero</option>
                                                      <option value="Marzo">Marzo</option>
                                                      <option value="Abril">Abril</option>
                                                      <option value="Mayo">Mayo</option>
                                                      <option value="Junio">Junio</option>
                                                      <option value="Julio">Julio</option>
                                                      <option value="Agosto">Agosto</option>
                                                      <option value="Septiembre">Septiembre</option>
                                                      <option value="Octubre">Octubre</option>
                                                      <option value="Noviembre">Noviembre</option>
                                                      <option value="Diciembre">Diciembre</option>
                            </select>
                          </div>  
                      </div>


                      <div class="form-group col-md-3">
                        <label for="inputPassword4">Descuento</label>
                          <div class="form-select-list">
                            <select class="form-control custom-select-value" name="idDescuento">
                                <?php 
                                      foreach ($lista_descuento as $elemento) {
                                        $select='';
                                        if ($elemento['idDescuento']==$ingreso->idDescuento) 
                                          $select=' selected';
                                        echo '<option '.$select.' value="'.$elemento['idDescuento'].'">'.$elemento['nombre_descuento'].'</option>';
                                      }
                                ?>
                                                                    
                            </select>
                          </div>                      
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputEmail4">Fecha de pago</label>
                        <input type="datetime" value="<?php echo $ingreso->fecha_registro;echo date("Y-m-d");?>" name="fecha_registro" class="form-control" required  readonly=»readonly» />  
                      </div>
                      
                    </div>

                    <div class="form-row">

                      <div class="form-group col-md-4">
                        <label for="inputPassword4">Importe</label>
                        <input type="text" value="<?php echo $ingreso->importe;?>" name="importe" class="form-control" required="" />
                      </div>

                      <div class="form-group col-md-2">
                        <label for="inputPassword4">Estado</label>
                          <div class="form-select-list">
                            <select class="form-control custom-select-value" name="estado_cobro" required>
                            <option value="<?php echo $ingreso->estado_cobro;?>"><?php echo $ingreso->estado_cobro;?></option>
                                                        <option value="Pagado">Pagado</option>
                                                        <option value="Cancelado">Cancelado</option>
                            </select>
                          </div>  
                      </div>

                      <div class="form-group col-md-6">
                        <label for="inputEmail4">Comentario</label>
                        <input type="text" value="<?php echo $ingreso->comentario;?>" name="comentario" class="form-control" required="" />
                      </div>

                    </div>
                    
                   <br> 

                    <div class="form-row">
                      <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-outline-primary" name="operacion" onclick="return confirmar()" value="<?php echo "$operacion";?>">Guardar</button>
                        <?php
                                                                      echo $boton_eliminar;
                                                                    ?>
                                                                 
                      </div>
                      <div class="form-group col-md-1">
                         <button type="submit" class="btn btn-outline-secondary" name="operacion" onclick="return confirmar()" value="<?php echo "$operacion";?>">Cancelar</button>  
                      </div>
                      <div class="form-group col-md-9">

                        
                      </div>
                    </div>
              </form>

                  </div> 
                   
                </div>
              </div>

              

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->

      <?php include_once ('../elementos/footer-admin.php');   ?>
     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../../vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/chart-area-demo.js"></script>
  <script src="../../js/demo/chart-pie-demo.js"></script>
  <script src="../../admin/funciones.js"></script>


</body>

</html>
