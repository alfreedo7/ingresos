<?php
session_start();
include_once('../../clases/seguridad.php');
$seg=new seguridad();
$seg->candado();
$seg->candado_permiso($_SESSION['idUsuario'], 'ADMIN');

include_once('../../clases/rol.php');

$rol=new rol();

include_once('../../clases/usuario.php');
$ObjUser=new usuario();
$ObjUser->idUsuario=$_SESSION['idUsuario'];
$ObjUser->obtener_usuario();



if(isset($_GET['id'])){
$operacion='Modificar';
$eliminar='<input name="operacion" type="submit" class="btn btn-custon-rounded-three btn-danger" onclick="return eliminar()" value="Eliminar"/>';
$rol->idRol=$_GET['id'];
$rol->obtener_rol();
}
else{
$operacion='Agregar';
$eliminar='';
}
?>


<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Sin Permiso || REDY </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="../../image/x-icon" href="../../img/idey.ico">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <!-- adminpro icon CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/adminpro-custon-icon.css">
    <!-- meanmenu icon CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/meanmenu.min.css">
    <!-- mCustomScrollbar CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/jquery.mCustomScrollbar.min.css">
    <!-- animate CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/animate.css">
    <!-- data-table CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/data-table/bootstrap-table.css">
    <link rel="stylesheet" href="../../css/data-table/bootstrap-editable.css">
    <!-- normalize CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/normalize.css">
    <!-- charts C3 CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/c3.min.css">
    <!-- forms CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/form/all-type-forms.css">
    <!-- switcher CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/switcher/color-switcher.css">
    <!-- style CSS
        ============================================ -->
    <link rel="stylesheet" href="../../style.css">
    <!-- responsive CSS
        ============================================ -->
    <link rel="stylesheet" href="../../css/responsive.css">
    <!-- modernizr JS
        ============================================ -->
    <script src="../../js/vendor/modernizr-2.8.3.min.js"></script>
    <!-- Color Css Files
        ============================================ -->
    <link rel="alternate stylesheet" type="text/css" href="../../css/switcher/color-one.css" title="color-one" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="../../css/switcher/color-two.css" title="color-two" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="../../css/switcher/color-three.css" title="color-three" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="../../css/switcher/color-four.css" title="color-four" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="../../css/switcher/color-five.css" title="color-five" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="../../css/switcher/color-six.css" title="color-six" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="../../css/switcher/color-seven.css" title="color-seven" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="../../css/switcher/color-eight.css" title="color-eight" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="../../css/switcher/color-nine.css" title="color-nine" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="../../css/switcher/color-ten.css" title="color-ten" media="screen" />

    <!--Iconos-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    
    <style>
        footer {
            
           
            border:1px solid #c9c9c9;
            position: fixed;
            bottom: 0;
            width: 100%;
            height: 40px;

            clear: both;
            background: linear-gradient( #f9f9f9 0px, #e9e9e9 100%);
            border: 1px solid #c9c9c9;
            border-bottom: none;
            border-radius: 8px 8px 0px 0px;
            color: #666;
            padding: 15px;
            text-align: center;
}
    </style>



</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Header top area start-->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="admin-logo">
                        <a href="../panel"><img src="../../img/logo/idey.png" alt="" />
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-0 col-xs-12">
                    
                </div>

                <div class="col-lg-4 col-md-9 col-sm-6 col-xs-12">
                    <div class="header-right-info">
                        <ul class="nav navbar-nav mai-top-nav header-right-menu">
                            <li class="nav-item">
                                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                    <span class="adminpro-icon adminpro-user-rounded header-riht-inf"><!-- <?php echo $ObjUser->nomUsuario;?>--></span>
                                    <span class="admin-name"><?php echo $ObjUser->nomUsuario; ?></span>
                                    <span class="author-project-icon adminpro-icon adminpro-down-arrow"></span>
                                </a>
                                <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated flipInX">
                                    <li><a href="../ver/perfil"><span class="far fa-user-circle author-log-ic"></span>Mi Perfil</a>
                                    </li>
                                    <li><a href="#"><span class="fas fa-sign-in-alt author-log-ic"></span>Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header top area end-->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-tabs custom-menu-wrap">
                        <li class="active"><a data-toggle="tab" href="#inicio">Inicio</a>
                        </li>
                        <li><a data-toggle="tab" href="#usuarios">Usuarios</a>
                        </li>
                        <li><a data-toggle="tab" href="#permisos">Permisos</a>
                        </li>
                        <li><a data-toggle="tab" href="#rol">Rol</a>
                        </li>
                        <li><a data-toggle="tab" href="#personafisica">Persona Física</a>
                        </li>
                        <li><a data-toggle="tab" href="#personafisica">Persona Moral</a>
                        </li>
                        <li><a data-toggle="tab" href="#eventos">Eventos Deportivos</a>
                        </li>
                        <li><a data-toggle="tab" href="#infraestructura">Infraestructura Deportiva</a>
                        </li>
                    </ul>
                    <div class="tab-content custom-menu-content">
                        <div id="inicio" class="tab-pane in active tab-custon-menu-bg animated flipInX">
                            <ul class="main-menu-dropdown">
                            </ul>
                        </div>
                        <div id="usuarios" class="tab-pane tab-custon-menu-bg animated flipInX">
                            <ul class="main-menu-dropdown">
                                <li><a href="../usuario/formulario_usuario">Agregar Usuario</a>
                                </li>
                                <li><a href="../usuario/listado_usuario">Usuarios Registrados</a>
                                </li>
                            </ul>
                        </div>
                        <div id="rol" class="tab-pane tab-custon-menu-bg animated flipInX">
                            <ul class="main-menu-dropdown">
                                <li><a href="../rol/formulario_rol">Agregar Rol</a>
                                </li>
                                <li><a href="../rol/listado_rol">Roles Registrados</a>
                                </li>
                            </ul>
                        </div>
                        <div id="permisos" class="tab-pane tab-custon-menu-bg animated flipInX">
                            <ul class="main-menu-dropdown">
                                <li><a href="../permiso/formulario_permiso">Agregar Permiso</a>
                                </li>
                                <li><a href="../permiso/listado_permiso">Lisado de Permisos</a>
                                </li>
                            </ul>
                        </div>
                        <div id="infraestructura" class="tab-pane tab-custon-menu-bg animated flipInX">
                            <ul class="main-menu-dropdown">
                                <li><a href="../deportiva/formulario_deportiva">Nuevo Registro</a>
                                </li>
                                <li><a href="../deportiva/listado_deportiva">Todos los Registros</a>
                                </li>
                            </ul>
                        </div>
                        <div id="eventos" class="tab-pane tab-custon-menu-bg animated flipInX">
                            <ul class="main-menu-dropdown">
                                <li><a href="../eventos/formulario_eventos">Nuevo Registro</a>
                                </li>
                                <li><a href="../eventos/listado_eventos">Todos los Registros</a>
                                </li>
                            </ul>
                        </div>
                        <div id="personamoral" class="tab-pane tab-custon-menu-bg animated flipInX">
                            <ul class="main-menu-dropdown">
                                <li><a href="../persona-moral/formulario_personamoral">Nuevo Registro</a>
                                </li>
                                <li><a href="../persona-moral/listado_personamoral">Todos los Registros</a>
                                </li>
                            </ul>
                        </div>
                        <div id="personafisica" class="tab-pane tab-custon-menu-bg animated flipInX">
                            <ul class="main-menu-dropdown">
                                <li><a href="../persona-fisica/formulario_personafisica">Nuevo Registro Persona Fisica</a>
                                </li>
                                <li><a href="../persona-fisica/listado_personafisica">Todos los Registros</a>
                                </li>
                                <li><a href="../listado/formulario_listado">Nuevo Registro Social</a>
                                </li>
                                <li><a href="../listado/listado_listado">Todos los Registros</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li><a data-toggle="collapse" data-target="#Charts" href="#">Inicio<span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                    <ul class="collapse dropdown-header-top">
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#demo" href="#">Usuarios<span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                    <ul id="demo" class="collapse dropdown-header-top">
                                        <li><a href="../usuario/formulario_usuario">Agregar</a>
                                        </li>
                                        <li><a href="../usuario/listado_usuario">Registrados</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#others" href="#">Permisos <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                    <ul id="others" class="collapse dropdown-header-top">
                                        <li><a href="../permiso/formulario_permiso">Agregar</a>
                                        </li>
                                        <li><a href="../permiso/listado_permiso">Listado</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#Miscellaneousmob" href="#">Rol <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                    <ul id="Miscellaneousmob" class="collapse dropdown-header-top">
                                        <li><a href="../rol/formulario_rol">Agregar</a>
                                        </li>
                                        <li><a href="../rol/listado_rol">Listado</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#Chartsmob" href="#">Per. Física <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                    <ul id="Chartsmob" class="collapse dropdown-header-top">
                                        <li><a href="#">Nuevo Registro</a>
                                        </li>
                                        <li><a href="#">Todos los Registros</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#Tablesmob" href="#">Per.Moral <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                    <ul id="Tablesmob" class="collapse dropdown-header-top">
                                        <li><a href="#">Nuevo Registro</a>
                                        </li>
                                        <li><a href="#">Todos los Registros</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#Tablesmob" href="#">Eventos <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                    <ul id="Tablesmob" class="collapse dropdown-header-top">
                                        <li><a href="basic-form-element.html">Nuevo Registro</a>
                                        </li>
                                        <li><a href="advance-form-element.html">Todos los Registros</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a data-toggle="collapse" data-target="#Appviewsmob" href="#">Infra. Dep. <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                    <ul id="Appviewsmob" class="collapse dropdown-header-top">
                                        <li><a href="basic-form-element.html">Nuevo Registro</a>
                                        </li>
                                        <li><a href="advance-form-element.html">Todos los Registros</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->

        <!-- Basic Form Start -->
    <div class="basic-form-area mg-b-40">
        <div class="container">
            
            <div class="row">
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline12-list shadow-reset mg-t-30">
                        <div class="sparkline12-hd">
                            <div class="main-sparkline12-hd">
                                <center><h1>Agregar Nuevo Rol</h1></center>
                                <!--<div class="sparkline12-outline-icon">
                                    <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                    <span><i class="fa fa-wrench"></i></span>
                                    <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>
                                </div>-->
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="all-form-element-inner">
                                            <form action="operaciones_rol.php" method="POST">
                                                <input type="hidden" value="<?php echo $rol->idRol;?>" name="idRol" class="form-control">
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <label class="login2 pull-right pull-right-pro">Nombre del Rol</label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <input type="text" value="<?php echo $rol->nomRol;?>" name="nomRol" class="form-control" required />
                                                        </div>
                                                    </div>
                                                </div>

                                                
                                                

                                                 
                                                <div class="form-group-inner">
                                                    <div class="login-btn-inner">
                                                        <div class="row">
                                                            <div class="col-lg-3"></div>
                                                            <div class="col-lg-9">
                                                                <div class="login-horizental cancel-wp pull-left">
                                                                    <button class="btn btn-custon-rounded-three btn-success" type="submit" name="operacion" onclick="return confirmar()" value="<?php echo "$operacion";?>">Enviar</button>
                                                                    <?php
                                                                      echo $eliminar;
                                                                    ?>
                                                                    <button class="btn btn-custon-rounded-three btn-default" type="submit" name="operacion" value="Cancelar">Cancelar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Basic Form End-->
   
    <!-- Footer Start-->
    <footer>

        <p>Copyright &#169; 2019 IDEY Derechos reservados. Programado por Alfredo Carmona</p>
        
    </footer>
    
    <!-- Footer End-->
    <!-- Color Switcher -->
    <!--<div class="ec-colorswitcher">
        <a class="ec-handle" href="#"><i class="fa fa-cog" aria-hidden="true"></i></a>
        <h3>Style Switcher</h3>
        <div class="ec-switcherarea">
            <div class="base-color">
                <h6>Background Color</h6>
                <ul class="ec-switcher">
                    <li>
                        <a href="#" class="cs-color-1 styleswitch" data-rel="color-one"></a>
                    </li>
                    <li>
                        <a href="#" class="cs-color-2 styleswitch" data-rel="color-two"></a>
                    </li>
                    <li>
                        <a href="#" class="cs-color-3 styleswitch" data-rel="color-three"></a>
                    </li>
                    <li>
                        <a href="#" class="cs-color-4 styleswitch" data-rel="color-four"></a>
                    </li>
                    <li>
                        <a href="#" class="cs-color-5 styleswitch" data-rel="color-five"></a>
                    </li>
                    <li>
                        <a href="#" class="cs-color-6 styleswitch" data-rel="color-six"></a>
                    </li>
                    <li>
                        <a href="#" class="cs-color-7 styleswitch" data-rel="color-seven"></a>
                    </li>
                    <li>
                        <a href="#" class="cs-color-8 styleswitch" data-rel="color-eight"></a>
                    </li>
                    <li>
                        <a href="#" class="cs-color-9 styleswitch" data-rel="color-nine"></a>
                    </li>
                    <li>
                        <a href="#" class="cs-color-10 styleswitch" data-rel="color-ten"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>-->
    <!-- Color Switcher end -->
    <!-- jquery
        ============================================ -->
    <script src="../../js/vendor/jquery-1.11.3.min.js"></script>
    <!-- bootstrap JS
        ============================================ -->
    <script src="../../js/bootstrap.min.js"></script>
    <!-- meanmenu JS
        ============================================ -->
    <script src="../../js/jquery.meanmenu.js"></script>
    <!-- mCustomScrollbar JS
        ============================================ -->
    <script src="../../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sticky JS
        ============================================ -->
    <script src="../../js/jquery.sticky.js"></script>
    <!-- scrollUp JS
        ============================================ -->
    <script src="../../js/jquery.scrollUp.min.js"></script>
    <!-- counterup JS
        ============================================ -->
    <script src="../../js/counterup/jquery.counterup.min.js"></script>
    <script src="../../js/counterup/waypoints.min.js"></script>
    <script src="../../js/counterup/counterup-active.js"></script>
    <!-- peity JS
        ============================================ -->
    <script src="../../js/peity/jquery.peity.min.js"></script>
    <script src="../../js/peity/peity-active.js"></script>
    <!-- sparkline JS
        ============================================ -->
    <script src="../../js/sparkline/jquery.sparkline.min.js"></script>
    <script src="../../js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
        ============================================ -->
    <script src="../../js/flot/jquery.flot.js"></script>
    <script src="../../js/flot/jquery.flot.tooltip.min.js"></script>
    <script src="../../js/flot/jquery.flot.spline.js"></script>
    <script src="../../js/flot/jquery.flot.resize.js"></script>
    <script src="../../js/flot/jquery.flot.pie.js"></script>
    <script src="../../js/flot/Chart.min.js"></script>
    <script src="../../js/flot/flot-active.js"></script>
    <!-- map JS
        ============================================ -->
    <script src="../../js/map/raphael.min.js"></script>
    <script src="../../js/map/jquery.mapael.js"></script>
    <script src="../../js/map/france_departments.js"></script>
    <script src="../../js/map/world_countries.js"></script>
    <script src="../../js/map/usa_states.js"></script>
    <script src="../../js/map/map-active.js"></script>
    <!-- data table JS
        ============================================ -->
    <script src="../../js/data-table/bootstrap-table.js"></script>
    <script src="../../js/data-table/tableExport.js"></script>
    <script src="../../js/data-table/data-table-active.js"></script>
    <script src="../../js/data-table/bootstrap-table-editable.js"></script>
    <script src="../../js/data-table/bootstrap-editable.js"></script>
    <script src="../../js/data-table/bootstrap-table-resizable.js"></script>
    <script src="../../js/data-table/colResizable-1.5.source.js"></script>
    <script src="../../js/data-table/bootstrap-table-export.js"></script>
    <!-- switcher JS
        ============================================ -->
    <script src="../../js/switcher/styleswitch.js"></script>
    <script src="../../js/switcher/switch-active.js"></script>
    <!-- main JS
        ============================================ -->
    <script src="../../js/main.js"></script>

    <script>
function confirmar()
{
    if(confirm('¿Estas seguro de agregar este nuevo rol?'))
    {
        return true;
    }
    else
    {
        return false;
    }   
}
</script>

<script>
function eliminar()
{
    if(confirm('¿Estas seguro de eliminar este rol ?'))
    {
        return true;
    }
    else
    {
        return false;
    }   
}
</script>
</body>

</html>


<div class="container">
    <div class="row">             



            <div class="alert alert-danger" role="alert">
  <h4 class="alert-heading">No tiene los permisos necesarios. </h4>
  <p>Usted no tiene los permisos suficientes para acceder a esta sección. </p>
  <hr>
  <div class=""><a href="../../admin/bienvenida/bienvenida.php"><input type="submit" value="Cancelar" class="btn btn-success"></div></a>
 
</div>
    </div>
  </div>