<?php 

session_start();
    include_once('../../clases/usuario.php');
    $objUser=new usuario();
    $objUser->idUsuario=$_SESSION['idUsuario'];
    $objUser->obtener_usuario();

    $objRol=new rol();
    $objRol->idRol=$objUser->idRol;
    $objRol=$objUser->obtener_rol();
?>
<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aceptado</title>
    
    <script src="../../js/jquery.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="../../css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="row">
         
			<div class="col-md-offset-3 col-md-6">
            <div class="">
                <h3>Bienvenido al sistema: <?php echo $objRol->nomRol.' '.$objUser->nomUsuario ?></h3>
            </div>
            <br>
            <br>
            <br>
            <br>

			</div>
		</div>
	</div>
</body>
</html>