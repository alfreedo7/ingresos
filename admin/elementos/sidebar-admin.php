<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../panel.php">
        <div class="sidebar-brand-icon ">
           <img src="../../img/idey.png">
        </div>
        <div class="sidebar-brand-text mx-3"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="../panel.php">
         
          <span>Menú Principal</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">


<?php 

       if ($ObjUser->idRol==24){
        echo '  <!-- Heading -->
      <div class="sidebar-heading">
        Opciones
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Administrador</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Usuario:</h6>
            <a class="collapse-item" href="../usuario/nuevo.php">Agregar Usuario</a>
            <a class="collapse-item" href="../usuario/ver">Ver Usuarios</a>
            <h6 class="collapse-header">Rol:</h6>
            <a class="collapse-item" href="../rol/nuevo">Agregar Rol</a>
            <a class="collapse-item" href="../rol/ver">Ver Rol</a>
            <h6 class="collapse-header">Permiso:</h6>
            <a class="collapse-item" href="../permiso/nuevo">Agregar Permiso</a>
            <a class="collapse-item" href="../permiso/ver">Ver Permisos</a>
          </div>
        </div>
      </li>';
       }else{
        echo'';
       }
       ?>

      



      

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Registros
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Alumnos" aria-expanded="true" aria-controls="Alumnos">
          <i class="fas fa-fw fa-file"></i>
          <span>Alumnos</span>
        </a>
        <div id="Alumnos" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Opciones:</h6>
             <a class="collapse-item" href="../alumnos/nuevo.php">Agregar Alumno</a>
             <a class="collapse-item" href="../alumnos/ver.php">Ver Alumnos</a>
             <a class="collapse-item" href="">Generar Reporte</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Ingresos" aria-expanded="true" aria-controls="Ingresos">
          <i class="fas fa-fw fa-file"></i>
          <span>Ingresos</span>
        </a>
        <div id="Ingresos" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Opciones:</h6>
             <a class="collapse-item" href="../ingresos/nuevo_ingreso.php">Realizar Pago</a>
             <a class="collapse-item" href="../ingresos/ver.php">ver ingresos</a>
             <a class="collapse-item" href="../tipo-ingreso/nuevo.php">Agregar Tipo de Ingreso</a>
             <a class="collapse-item" href="../tipo-ingreso/ver.php">Ver Tipo de Ingreso</a>
             <a class="collapse-item" href="">Generar Reporte</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Agregar-Clases" aria-expanded="true" aria-controls="Agregar-Clases">
          <i class="fas fa-fw fa-file"></i>
          <span>Clases</span>
        </a>
        <div id="Agregar-Clases" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Opciones:</h6>
             <a class="collapse-item" href="../clase/nuevo.php">Registrar Clase</a>
             <a class="collapse-item" href="../clase/ver.php">Ver clases</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Reg-Unid" aria-expanded="true" aria-controls="Reg-Unid">
          <i class="fas fa-fw fa-file"></i>
          <span>Unidades Deportivas</span>
        </a>
        <div id="Reg-Unid" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Opciones:</h6>
             <a class="collapse-item" href="../unidad-deportiva/nuevo.php">Registrar Unidad Deportiva</a>
             <a class="collapse-item" href="../unidad-deportiva/ver.php">Ver Unidades Deportivas</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Descuentos" aria-expanded="true" aria-controls="Descuentos">
          <i class="fas fa-fw fa-file"></i>
          <span>Descuentos</span>
        </a>
        <div id="Descuentos" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Opciones:</h6>
             <a class="collapse-item" href="../descuentos/nuevo.php">Registrar Descuento</a>
             <a class="collapse-item" href="../descuentos/ver.php">Ver Descuentos</a>
          </div>
        </div>
      </li>

       <!-- Nav Item - Pages Collapse Menu -->
       <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Estatus" aria-expanded="true" aria-controls="Estatus">
          <i class="fas fa-fw fa-file"></i>
          <span>Estatus</span>
        </a>
        <div id="Estatus" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Opciones:</h6>
             <a class="collapse-item" href="../estatus/nuevo.php">Agregar Estatus</a>
             <a class="collapse-item" href="../estatus/ver.php">Ver Estatus</a>
          </div>
        </div>
      </li>
     

      

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
