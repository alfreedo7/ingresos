<?php
session_start();
include_once('../../clases/seguridad.php');
$seg=new seguridad();
$seg->candado("../../login.php");
$seg->candado_permiso($_SESSION['idUsuario'], 'SUPADMIN');


include_once('../../clases/rol.php');

$rol=new rol();

include_once('../../clases/usuario.php');
$ObjUser=new usuario();
$ObjUser->idUsuario=$_SESSION['idUsuario'];
$ObjUser->obtener_usuario();



if(isset($_GET['id'])){
$operacion='Modificar';
$eliminar='<input name="operacion" type="submit" class="btn btn-danger" onclick="return eliminar()" value="Eliminar"/>';
$rol->idRol=$_GET['id'];
$rol->obtener_rol();
}
else{
$operacion='Agregar';
$eliminar='';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="shortcut icon" type="image/x-icon" href="../../img/idey.ico">

  <title>Agregar Rol || REDY</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php include_once ('../elementos/sidebar-admin.php'); ?>
    

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->

        <?php include_once ('../elementos/topbar-admin.php');  ?>
        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
           

           

           

          <!-- Content Row -->
          <div class="row">

            

            <div class="col-lg-12 mb-4">

              <!-- Illustrations -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Agregar Rol</h6>
                </div>
                <div class="card-body">
                  <div class="text-center">

              <form action="operaciones_rol.php" method="POST">

                <input type="hidden" value="<?php echo $rol->idRol;?>" name="idRol" class="form-control">
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputEmail4">Nombre del rol</label>
                        <input type="text" value="<?php echo $rol->nomRol;?>" name="nomRol" class="form-control" required />
                      </div>
                      <div class="form-group col-md-6">
                         
                      </div>
                    </div>
                    
                    
                   <br> 

                    <div class="form-row">
                      <div class="form-group col-md-1">
                        <button type="submit" class="btn btn-outline-primary" name="operacion" onclick="return confirmar()" value="<?php echo "$operacion";?>">Guardar</button>
                        <?php
                                                                      echo $eliminar;
                                                                    ?>
                                                                 
                      </div>
                      <div class="form-group col-md-1">
                         <button type="submit" class="btn btn-outline-secondary" name="operacion" onclick="return confirmar()" value="<?php echo "$operacion";?>">Cancelar</button>  
                      </div>
                      <div class="form-group col-md-10">

                        
                      </div>
                    </div>
              </form>

                  </div> 
                   
                </div>
              </div>

              

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->

      <?php include_once ('../elementos/footer-admin.php');   ?>
     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 

 <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../../vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/chart-area-demo.js"></script>
  <script src="../../js/demo/chart-pie-demo.js"></script>



</body>

</html>
