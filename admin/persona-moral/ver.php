<?php
session_start();
include_once('../../clases/seguridad.php');
$seg=new seguridad();
$seg->candado("../../login.php");
$seg->candado_permiso($_SESSION['idUsuario'], 'SUPADMIN');

if(isset($_POST['logOut'])){
    $seg->cerrar_sesion("../../login.php");
} 


include_once('../../clases/usuario.php');
$ObjUser=new usuario();
$ObjUser->idUsuario=$_SESSION['idUsuario'];
$ObjUser->obtener_usuario();

$usuario=new usuario();
$rol=new rol();
$lista_usuario=$usuario->listar_usuario();
?>


<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="shortcut icon" type="image/x-icon" href="../../img/idey.ico">

  <title>Ver Usuarios || REDY</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php include_once ('../elementos/sidebar-admin.php'); ?>
    

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->

        <?php include_once ('../elementos/topbar-admin.php');  ?>
        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
           <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Usuarios Registrados</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Nombre</th>
                      <th>Usuario</th>
                      
                      <th>Departamento</th>
                      <th>Rol</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                            foreach ($lista_usuario as $elemento) {             
                                $rol->idRol=$elemento['idRol'];
                                $rol->obtener_rol();
                                echo '<tr>
                                <td>'.$elemento['idUsuario'].'</td>
                                <td>'.$elemento['nomUsuario'].'</td>
                                
                                <td>'.$elemento['email'].'</td>
                                <td>'.$elemento['departamento'].'</td>
                                
                                <td>'.$rol->nomRol.'</td>
                                <td><center><a href="editar.php?id='.$elemento['idUsuario'].'"><img src="../../img/svg/edit-2.svg"></a></center></td>

                                  


                                </tr>';
                            }
                            ?>
                   
                   
                  </tbody>
                </table>
              </div>
            </div>
          </div>
           


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->

      <?php include_once ('../elementos/footer-admin.php');   ?>
     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../../vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/chart-area-demo.js"></script>
  <script src="../../js/demo/chart-pie-demo.js"></script>

   <!-- Page level plugins -->
  <script src="../../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/datatables-demo.js"></script>

</body>

</html>
