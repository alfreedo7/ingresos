<?php
session_start();
include_once('../../clases/seguridad.php');
$seg=new seguridad();
$seg->candado('../../login.php');
$seg->candado_permiso($_SESSION['idUsuario'], 'ADMIN');

if(isset($_POST['logOut'])){
  $seg->cerrar_sesion("../../login.php");
}

date_default_timezone_set('America/Merida');

include_once('../../clases/personamoral.php');

$personamoral=new personamoral();

include_once('../../clases/usuario.php');
$ObjUser=new usuario();
$ObjUser->idUsuario=$_SESSION['idUsuario'];
$ObjUser->obtener_usuario();



if(isset($_GET['id'])){
  $operacion='Modificar';
  $eliminar='<input name="operacion" type="submit" class="btn btn-outline-danger" onclick="return eliminar()" value="Eliminar"/>';
  $personamoral->idPm=$_GET['id'];
  $personamoral->obtener_personamoral();
}
else{
  $operacion='Agregar';
  $eliminar='';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" type="image/x-icon" href="../../img/idey.ico">

  <title>Nuevo Registro || REDY</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php include_once ('../elementos/sidebar-admin.php'); ?>
    

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->

        <?php include_once ('../elementos/topbar-admin.php');  ?>
        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->






          <!-- Content Row -->
          <div class="row">



            <div class="col-lg-12 mb-4">

              <!-- Illustrations -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Nuevo Registro Persona Moral</h6>
                </div>
                <div class="card-body">
                  <div class="text-center">

                    <form action="operaciones_personamoral.php" method="POST" enctype="multipart/form-data">

                      <input type="hidden" value="<?php echo $personamoral->idPm;?>" name="idPm" class="form-control">

                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="inputEmail4">Razon Social*</label>
                          <input type="text" value="<?php echo $personamoral->razon;?>" name="razon" class="form-control" required />  
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputEmail4">Siglas*</label>
                          <input type="text" value="<?php echo $personamoral->siglas;?>" name="siglas" class="form-control" required />  
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">RFC*</label>
                          <input type="text" value="<?php echo $personamoral->rfc;?>" name="rfc" class="form-control" required />   
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="inputEmail4">Representante Legal*</label>
                          <input type="text" value="<?php echo $personamoral->repLegal;?>" name="repLegal" class="form-control" required />  
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="inputEmail4">CURP*</label>
                          <input type="text" value="<?php echo $personamoral->curp;?>" name="curp" class="form-control" required />
                        </div>
                        <div class="form-group col-md-2">
                          <label for="inputPassword4">Fecha Nacimiento*</label>
                          <input type="date" value="<?php echo $personamoral->nacimiento;?>" name="nacimiento" class="form-control" required />  
                        </div>
                        <div class="form-group col-md-2">
                          <label for="inputPassword4">Edad*</label>
                          <input type="number" value="<?php echo $personamoral->edad;?>" name="edad" class="form-control" required />  
                        </div>
                        <div class="form-group col-md-1">
                          <label for="inputPassword4">Género*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="genero">
                            <option value="<?php echo $personamoral->genero;?>"><?php echo $personamoral->genero;?></option>
                                                    <option value="H">H</option>
                                                    <option value="M">M</option>

                          </select>
                        </div>  
                        </div>


                        <div class="form-group col-md-3">

                          <label for="inputEmail4">Cargo*</label>
                          <input type="text" value="<?php echo $personamoral->cargo;?>" name="cargo" class="form-control" required />
                        </div>
                      </div>



                      <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="inputEmail4">Paterno*</label>
                          <input type="text" value="<?php echo $personamoral->paterno;?>" name="paterno" class="form-control" required />
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Materno*</label>
                          <input type="text" value="<?php echo $personamoral->materno;?>" name="materno" class="form-control" required />  
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Nombre(s)*</label>
                          <input type="text" value="<?php echo $personamoral->nombre;?>" name="nombre" class="form-control" required />  
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="form-group col-md-2">
                          <label for="inputEmail4">Código Postal*</label>
                          <input type="number" value="<?php echo $personamoral->cp;?>" name="cp" class="form-control" required />
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Teléfono*</label>
                          <input type="number" value="<?php echo $personamoral->telefono;?>" name="telefono" class="form-control" required />  
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Dirección*</label>
                           <input type="text" value="<?php echo $personamoral->direccion;?>" name="direccion" class="form-control" required />  
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="inputEmail4">Correo*</label>
                          <input type="email" value="<?php echo $personamoral->correo;?>" name="correo" class="form-control" required />
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Escolaridad*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="escolaridad">
                            <option value="<?php echo $personamoral->escolaridad;?>"><?php echo $personamoral->escolaridad;?></option>
                                    <option value="Primaria">Primaria</option>
                                    <option value="Secundaria">Secundaria</option>
                                    <option value="Media Superior">Media Superior</option>
                                    <option value="Superior">Superior</option>
                                    <option value="Posgrado">Posgrado</option>

                          </select>
                        </div>  
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Estado Escolaridad*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="estadoEsc">
                            <option value="<?php echo $personamoral->estadoEsc;?>"><?php echo $personamoral->estadoEsc;?></option>
                                <option value="Cursando">Cursando</option>
                                <option value="Terminada">Terminada</option>
                                <option value="Trunca">Trunca</option>

                          </select>
                        </div>  
                        </div>
                      </div>

                      <br>
                      <br>

                      <div class="form-row">
                        <div class="form-group col-md-1">
                           
                        </div>
                        <div class="form-group col-md-5">
                          <label for="inputPassword4">Función*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="funcion">
                           <option value="<?php echo $personamoral->funcion;?>"><?php echo $personamoral->funcion;?></option>
                            <option value="Social">Social</option>
                            <option value="Privado">Privado</option>
                            <option value="Otro">Otro</option>

                          </select>
                        </div> 
                        </div>

                         <div class="form-group col-md-5">
                          <label for="inputPassword4">Acredita*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="acreditFuncion">
                           <option value="<?php echo $personamoral->acreditFuncion;?>"><?php echo $personamoral->acreditFuncion;?></option>
                        <option value="Constancia de afiliacion">Constancia de afiliación</option>
                        <option value="Constancia del SAT">Constancia del SAT</option>
                        <option value="Constancia Vecinal">Constancia Vecinal</option>
                        <option value="Otro">Otro</option>

                          </select>
                        </div> 
                        </div>

                        <div class="form-group col-md-1">
                            
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="form-group col-md-1">
                           
                        </div>
                        <div class="form-group col-md-5">
                          <label for="inputPassword4">Deporte*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="deporte">
                           <option value="<?php echo $personamoral->deporte;?>"><?php echo $personamoral->deporte;?></option>
                    <option value="Aguas Abiertas">Aguas Abiertas</option>
                    <option value="Ajedrez">Ajedrez</option>
                    <option value="Atletismo">Atletismo</option>
                    <option value="Automovilismo">Automovilismo</option>
                    <option value="Badminton">Badminton</option>
                    <option value="Baloncesto">Baloncesto</option>
                    <option value="Boliche">Boliche</option>
                    <option value="Boxeo">Boxeo</option>
                    <option value="Canotaje">Canotaje</option>
                    <option value="Charrería">Charrería</option>
                    <option value="Clavados">Clavados</option>
                    <option value="Ciclismo">Ciclismo</option>
                    <option value="Esgrima">Esgrima</option>
                    <option value="Fisicoconstructivismo">Fisicoconstructivismo</option>
                    <option value="Fútbol">Fútbol</option>
                    <option value="Gimnasia Olimpica">Gimnasia Olimpica</option>
                    <option value="Gimnasia Rítmica">Gimnasia Rítmica</option>
                    <option value="Gimnasia Trampolin">Gimnasia Trampolin</option>
                    <option value="Gimnasia Aerobica">Gimnasia Aerobica</option>
                    <option value="Handball">Handball</option>
                    <option value="Hockey">Hockey</option>
                    <option value="Hockey Sobre Hielo">Hockey Sobre Hielo</option>
                    <option value="Judo">Judo</option>
                    <option value="Juegos Autóctonos y Tradicionales">Juegos Autóctonos y Tradicionales</option>
                    <option value="Karate Do">Karate Do</option>
                    <option value="Levantamiento de Pesas">Levantamiento de Pesas</option>
                    <option value="Lima Lama">Lima Lama</option>
                    <option value="Luchas Asociadas">Luchas Asociadas</option>
                    <option value="Motociclismo">Motociclismo</option>
                    <option value="Natación">Natación</option>
                    <option value="Patines sobre ruedas">Patines sobre ruedas</option>
                    <option value="Pentatlón Moderno">Pentatlón Moderno</option>
                    <option value="Pentatlón deportivo militarizado">Pentatlón deportivo militarizado</option>
                    <option value="Remo">Remo</option>
                    <option value="Rugbi">Rugbi</option>
                    <option value="Squash">Squash</option>
                    <option value="Softbol">Softbol</option>
                    <option value="Tabla Vela">Tabla Vela</option>
                    <option value="Tiro con arco">Tiro con arco</option>
                    <option value="Tiro Deportivo">Tiro Deportivo</option>
                    <option value="Voleibol">Voleibol</option>
                    <option value="Voleibol Playa">Voleibol Playa</option>
                    <option value="Wu Shu">Wu Shu</option>
                    <option value="Otro">Otro</option>

                          </select>
                        </div> 
                        </div>

                         <div class="form-group col-md-5">
                          <label for="inputPassword4">Acredita*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="acreditDepor">
                           <option value="<?php echo $personamoral->acreditDepor;?>"><?php echo $personamoral->acreditDepor;?></option>
                <option value="Constancia de afiliacion">Constancia de afiliación</option>
                <option value="Constancia del SAT">Constancia del SAT</option>
                <option value="Constancia Vecinal">Constancia Vecinal</option>
                <option value="Otro">Otro</option>

                          </select>
                        </div> 
                        </div>

                        <div class="form-group col-md-1">
                            
                        </div>
                      </div>


                       <div class="form-row">
                        <div class="form-group col-md-3">
                          <label for="inputPassword4">Publica/Privada</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="tipo">
                           <option value="<?php echo $personamoral->tipo;?>"><?php echo $personamoral->tipo;?></option>
                           <option value="Publica">Pública</option>
                           <option value="Privada">Privada</option>

                          </select>
                        </div> 
                        </div>

                        <div class="form-group col-md-3">
                          <label for="inputPassword4">Organismo al que pertenece*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="organismo">
                            <option value="<?php echo $personamoral->organismo;?>"><?php echo $personamoral->organismo;?></option>
                            <option value="Gobierno del Estado">Gobierno del Estado</option>
                            <option value="Municipio">Municipio</option>
                            <option value="Asociacion Civil Organizada">Asociacion Civil Organizada</option>
                            <option value="Asociación Deportiva Estatal">Asociación Deportiva Estatal</option>
                            <option value="Corporativo Empresarial">Corporativo Empresarial</option>
                            <option value="Liga">Liga</option>
                            <option value="Club">Club</option>
                            <option value="Colonia">Colonia</option>
                            <option value="Otro">Otro</option>

                          </select>
                        </div> 
                        </div>

                         <div class="form-group col-md-3">
                          <label for="inputPassword4">Número Equipos*</label>
                          <input type="number" value="<?php echo $personamoral->equipos;?>" name="equipos" class="form-control" required /> 
                        </div>

                        <div class="form-group col-md-3">
                           <label for="inputPassword4">Número Deportistas*</label>
                          <input type="number" value="<?php echo $personamoral->deportistas;?>" name="deportistas" class="form-control" required /> 
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="inputEmail4">Fecha de Recepcion*</label>
                          <input id="fechaRecepcion" type="date" value="<?php echo $personamoral->fechaRecepcion;?>" name="fechaRecepcion" class="form-control" required /> 
                        </div>
                      </div>
                      <br>

                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2>Documentos Entregados</h2>
                        </div>
                      </div>
                      <br>

                       <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Acta Constitutiva*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="acta">
                           <option value="<?php echo $personamoral->acta;?>"><?php echo $personamoral->acta;?></option>
                           <option value="Entregada">Entregada</option>
                           <option value="Faltante">Faltante</option>

                          </select>
                        </div> 
                        </div>

                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Acta Electiva*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="acta2">
                            <option value="<?php echo $personamoral->acta2;?>"><?php echo $personamoral->acta2;?></option>
                            <option value="Entregada">Entregada</option>
                            <option value="Faltante">Faltante</option>

                          </select>
                        </div> 
                        </div>

                         <div class="form-group col-md-4">
                          <label for="inputPassword4">Constancia SAT*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="sat">
                            <option value="<?php echo $personamoral->sat;?>"><?php echo $personamoral->sat;?></option>
                            <option value="Entregada">Entregada</option>
                            <option value="Faltante">Faltante</option>

                          </select>
                        </div> 
                        </div>
                      </div>

                        <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Comprobante Domiciliario*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="comprobante">
                           <option value="<?php echo $personamoral->comprobante;?>"><?php echo $personamoral->comprobante;?></option>
                           <option value="Entregada">Entregada</option>
                           <option value="Faltante">Faltante</option>

                          </select>
                        </div> 
                        </div>

                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Lista Afiliados/Socios*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="listado">
                            <option value="<?php echo $personamoral->listado;?>"><?php echo $personamoral->listado;?></option>
                            <option value="Entregada">Entregada</option>
                            <option value="Faltante">Faltante</option>

                          </select>
                        </div> 
                        </div>

                         <div class="form-group col-md-4">
                          <label for="inputPassword4">Programa Anual Actividades*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="programa">
                           <option value="<?php echo $personamoral->programa;?>"><?php echo $personamoral->programa;?></option>
                           <option value="Entregada">Entregada</option>
                           <option value="Faltante">Faltante</option>

                          </select>
                        </div> 
                        </div>
                      </div>

                      <br>

                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <h2>Documentación</h2>
                        </div>
                      </div>
                      <br>

                      <div class="form-row">
                        <div class="form-group col-md-6"> 
                           <label>Documentación(.zip)*</label>
                            <input type="file" id="archivoInput" onchange="return validarExt()" value="<?php echo $personamoral->documentos;?>" name="documentos" class="form-control"  />


                        </div>
                        <div class="form-group col-md-6"> 
                           <label>Foto(jpg)*</label>
                           <input type="file" id="archivoInputFoto" onchange="return validarExtFoto()" value="<?php echo $personamoral->foto;?>" name="foto" class="form-control"   />


                        </div>
                      </div>


                      <div class="form-row">
                        <div class="form-group col-md-6"> 
                            


                        </div>
                        <div class="form-group col-md-6"> 
                           <div id="visorArchivoFoto" class="col-lg-6">


                        </div>
                      </div>
                    </div>

                     <div class="form-row">
                         
                        <div class="form-group col-md-12" style="text-align: right;"> 
                           <label>(*) Datos Obligatorios.</label>


                        </div>
                      </div>
                    






                       

                    
                    <br> 

                    <div class="form-row">
                      <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-outline-primary" name="operacion" onclick="return confirmar()" value="<?php echo "$operacion";?>">Guardar</button>
                        <?php
                        echo $eliminar;
                        ?>

                      </div>
                      <div class="form-group col-md-1">
                       <button type="submit" class="btn btn-outline-secondary" name="operacion" onclick="return confirmar()" value="<?php echo "$operacion";?>">Cancelar</button>  
                     </div>
                     <div class="form-group col-md-9">


                     </div>
                   </div>

                 </form>

               </div> 

             </div>
           </div>



         </div>
       </div>

     </div>
     <!-- /.container-fluid -->

   </div>
   <!-- End of Main Content -->

   <!-- Footer -->

   <?php include_once ('../elementos/footer-admin.php');   ?>

   <!-- End of Footer -->

 </div>
 <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>



<!-- Bootstrap core JavaScript-->
<script src="../../vendor/jquery/jquery.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="../../js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="../../vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="../../js/demo/chart-area-demo.js"></script>
<script src="../../js/demo/chart-pie-demo.js"></script>

 <script>
            function confirmar(){
                if(confirm('¿Estas seguro de agregar este nuevo registro?')){
                    return true;
                }
                else{
                    return false;
                }
            }

        </script>

        <script>
            function eliminar(){
                if(confirm('¿Estas seguro de eliminar este registro?')){
                    return true;
                }
                else{
                    return false;
                }
            }
        </script>

        <script type="text/javascript">

            function validarExt()
            {
                var archivoInput = document.getElementById('archivoInput');
                var archivoRuta = archivoInput.value;
                var extPermitidas = /(.zip|.rar)$/i;
                if(!extPermitidas.exec(archivoRuta)){
                    alert('Asegurese de haber seleccionado un archivo comprimido (zip o rar)');
                    archivoInput.value = '';
                    return false;
                }

                else
                {
        //PRevio del PDF
        if (archivoInput.files && archivoInput.files[0]) 
        {
            var visor = new FileReader();
            visor.onload = function(e) 
            {
                document.getElementById('visorArchivo').innerHTML = 
                '<embed src="'+e.target.result+'" width="500" height="375" />';
            };
            visor.readAsDataURL(archivoInput.files[0]);
        }
    }
}
</script>

<script type="text/javascript">

    function validarExtFoto()
    {
        var archivoInputFoto = document.getElementById('archivoInputFoto');
        var archivoRuta = archivoInputFoto.value;
        var extPermitidas = /(.jpg)$/i;
        if(!extPermitidas.exec(archivoRuta)){
            alert('Asegurese de haber seleccionado una imagen jpg ');
            archivoInputFoto.value = '';
            return false;
        }

        else
        {
        //PRevio del PDF
        if (archivoInputFoto.files && archivoInputFoto.files[0]) 
        {
            var visor = new FileReader();
            visor.onload = function(e) 
            {
                document.getElementById('visorArchivoFoto').innerHTML = 
                '<embed src="'+e.target.result+'" width="100" height="100" />';
            };
            visor.readAsDataURL(archivoInputFoto.files[0]);
        }
    }
}
</script>


</body>

</html>
