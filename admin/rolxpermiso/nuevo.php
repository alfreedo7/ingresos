<?php
session_start();
include_once('../../clases/seguridad.php');
$seg=new seguridad();
$seg->candado("../../login.php");
$seg->candado_permiso($_SESSION['idUsuario'], 'SUPADMIN');
include_once('../../clases/permiso.php');
include_once('../../clases/rolxpermiso.php');
include_once('../../clases/usuario.php');
$ObjUser=new usuario();
$ObjUser->idUsuario=$_SESSION['idUsuario'];
$ObjUser->obtener_usuario();

$permiso=new permiso();
$lista=$permiso->listar_permiso();
$rxp=new rolxpermiso();
$rxp->idRol=$_GET['id'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="shortcut icon" type="image/x-icon" href="../../img/icono.ico">

  <title>Asignar permisos</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php include_once ('../elementos/sidebar-admin.php'); ?>
    

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->

        <?php include_once ('../elementos/topbar-admin.php');  ?>
        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
           

           

           

          <!-- Content Row -->
          <div class="row">

            

            <div class="col-lg-12 mb-4">

              <!-- Illustrations -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Asignar permiso</h6>
                </div>
                <div class="card-body">
                  <div class="text-center">

              <form action="operaciones_rolxpermiso.php" method="POST">

                <input type="hidden" name="idRol" value="<?php echo $_GET['id'];?>"/>
                    <div class="form-row">
                      <table class="table">
                        <?php
                        foreach($lista as $elemento){
                            $rxp->idPermiso=$elemento['idPermiso'];
                            $check='';
                            $bandera=$rxp->validar_rolxpermiso();
                            if($bandera==1){
                                $check=' checked ';
                            }
                            echo '<tr>
                                  <td>
                                  <input class="form-check-input" value="'.$elemento['idPermiso'].'" '.$check.' 
                                  name="idPermiso[]" type="checkbox">
                                  </td>
                                  <td>'.$elemento['nomPermiso'].'</td>                                
                                  </tr>';
                        }
                        ?>
                    </table>
                    </div>
                    
                    
                   <br> 

                    <div class="form-row">
                      <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-primary" name="operacion" value="Asignar">Asignar</button>
                       
                                                                 
                      </div>
                      <div class="form-group col-md-1">
                         <button type="submit" class="btn btn-light" name="operacion" value="Cancelar">Cancelar</button>  
                      </div>
                      <div class="form-group col-md-9">

                        
                      </div>
                    </div>
              </form>

                  </div> 
                   
                </div>
              </div>

              

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->

      <?php include_once ('../elementos/footer-admin.php');   ?>
     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../../vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/chart-area-demo.js"></script>
  <script src="../../js/demo/chart-pie-demo.js"></script>



</body>

</html>
