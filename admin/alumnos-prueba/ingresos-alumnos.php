<?php
 

 

include_once('../../clases/ingreso.php');

$ingreso= new ingreso();
$alumno=new alumno();
$unidad= new unidad();
$clases= new clases();
$tipo= new tipo();
$descuento= new descuento();
$lista_ingreso=$ingreso->listar_ingreso();

if(isset($_GET['idAlumno'])){
    $ingreso->idAlumno=$_GET['idAlumno'];
    $lista_ingreso=$ingreso->listar_ingreso_alumno();
}
?>


<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="shortcut icon" type="image/x-icon" href="../../img/idey.ico">

  <title>Ver registro de ingresos || REDY</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

     
    <br>

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->

        
        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
           <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Registro de Ingresos</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTableA" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Alumno</th>
                      <th>Tipo</th>
                      <th>Comentario</th>
                      <th>Fecha de registro</th>
                      <th>Unidad Deportiva</th>
                      <th>Clase</th>
                      <th>Mes</th>
                      <th>Descuento</th>
                      <th>Importe</th>
                      <th>Estado de cobro</th>
                      <th>Editar</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                            foreach ($lista_ingreso as $elemento) {             
                                $alumno->idAlumno=$elemento['idAlumno'];
                                $alumno->obtener_alumno();

                                $unidad->idUnidad=$elemento['idUnidad'];
                                $unidad->obtener_unidad();

                                $clases->idClase=$elemento['idClase'];
                                $clases->obtener_clases();

                                $tipo->idTipo=$elemento['idTipo'];
                                $tipo->obtener_tipo();

                                $descuento->idDescuento=$elemento['idDescuento'];
                                $descuento->obtener_descuento();
                                echo '<tr>
                                <td>'.$elemento['idIngreso'].'</td>
                                <td>'.$alumno->nomalumno.'</td>
                                <td>'.$tipo->nombre.'</td>
                                <td>'.$elemento['comentario'].'</td>
                                <td>'.$elemento['fecha_registro'].'</td>
                                <td>'.$unidad->nombre_unidad.'</td>
                                <td>'.$clases->nombre_clase.'</td>
                                <td>'.$elemento['mes'].'</td>
                                <td>'.$descuento->nombre_descuento.'</td>
                                <td>'.$elemento['importe'].'</td>
                                <td>'.$elemento['estado_cobro'].'</td>




                                
                                
                                <td><center><a href="editar.php?id='.$elemento['idIngreso'].'"><img src="../../img/svg/edit-2.svg"></a></center></td>

                                  


                                </tr>';
                            }
                            ?>
                   
                   
                  </tbody>
                </table>
              </div>
            </div>
          </div>
           


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->

      <?php include_once ('../elementos/footer-admin.php');   ?>
     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../../vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/chart-area-demo.js"></script>
  <script src="../../js/demo/chart-pie-demo.js"></script>

   <!-- Page level plugins -->
  <script src="../../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/datatables-demo.js"></script>

  <script>
    $(document).ready(function() {
        $('#dataTableA').DataTable({
            responsive: true,
            language: {
                url: '../../vendor/datatables/es-ar.json' //Ubicacion del archivo con el json del idioma.
            }
        });
    });
 </script>

</body>

</html>
