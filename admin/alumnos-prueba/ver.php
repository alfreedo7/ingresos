<?php
session_start();

include_once('../../clases/seguridad.php');
$seg=new seguridad();
$seg->candado("../../login.php");
$seg->candado_permiso($_SESSION['idUsuario'], 'ADMIN');

if(isset($_POST['logOut'])){
    $seg->cerrar_sesion("../../login.php");
} 


include_once('../../clases/usuario.php');
$ObjUser=new usuario();
$ObjUser->idUsuario=$_SESSION['idUsuario'];
$ObjUser->obtener_usuario();

include_once('../../clases/alumno.php');

$alumno=new alumno();
$unidad= new unidad();
$estatus= new estatus();
$lista_alumno=$alumno->listar_alumno();
?>


<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="shortcut icon" type="image/x-icon" href="../../img/idey.ico">

  <title>Ver Usuarios || Ingresos</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php include_once ('../elementos/sidebar-admin.php'); ?>
    

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->

        <?php include_once ('../elementos/topbar-admin.php');  ?>
        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
           <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Alumnos Registrados</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTableA" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Nombre</th>
                      <th>Edad</th>
                      <th>Dirección</th>
                      <th>Teléfono</th>
                      <th>Unidad Deportiva</th>
                      <th>Fecha de Nacim.</th>
                      <th>Lugar de Nacim.</th>
                      <th>Genero</th>
                      <th>Tipo de Sangre</th>
                      <th>Estatus</th>
                      <th>Motivo</th>
                      <th>Editar</th>
                      <th>Detalles</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                            foreach ($lista_alumno as $elemento) {             
                             //$alumno->idAlumno=$elemento['idAlumno'];
                              //$alumno->obtener_alumno();
                                $unidad->idUnidad=$elemento['idUnidad'];
                                $unidad->obtener_unidad();
                                $estatus->idEstatus=$elemento['idEstatus'];
                                $estatus->obtener_estatus();
                                echo '<tr>

                                <td>'.$elemento['idAlumno'].'</td>
                                <td>'.$elemento['nomalumno'].'</td>
                                <td>'.$elemento['edad'].'</td>
                                <td>'.$elemento['direccion'].'</td>
                                <td>'.$elemento['telefono'].'</td>
                                <td>'.$unidad->nombre_unidad.'</td>
                                <td>'.$elemento['fecha_nac'].'</td>
                                <td>'.$elemento['lugar_nac'].'</td>
                                <td>'.$elemento['genero'].'</td>
                                <td>'.$elemento['tipo_sangre'].'</td>
                                <td>'.$estatus->nombre_estatus.'</td>
                                <td>'.$elemento['motivo'].'</td>

                              




                                
                                
                                <td><center><a href="editar.php?id='.$elemento['idAlumno'].'"><img src="../../img/svg/edit-2.svg"></a></center></td>
                               
                                <td><center><img src="../../img/svg/arrow-down-circle.svg"></a></center></td>

<<<<<<< HEAD
                                <td><button type="button" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" data-toggle="modal" data-target="#modalConfirmar'.$elemento['idAlumno'].'"> </td>
=======
                                <td><a href="ver-ingresos.php?idAlumno='.$elemento['idAlumno'].'" class="fancybox fancybox.iframe"> <img src="../../img/svg/file-text.svg"></a></td>



                                


>>>>>>> 22a457ee132936b0bceedc5eb060949422f5decd

                                <td><a href="ver-ingresos.php?idAlumno='.$elemento['idAlumno'].'" target="_blank"><img src="../../img/svg/file-text.svg"></a></td> 


                                  


                                </tr>';
                            }
                            ?>
                   
                   
                  </tbody>
                </table>
              </div>
            </div>
          </div>
           


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
<<<<<<< HEAD
    <?php foreach ($lista_alumno as $elemento) {?>
      <div class="modal fade" id="modalConfirmar<?php echo $elemento['idAlumno']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">          
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Confirmar</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div>
              <?php 
                
                $_GET['idAlumno'] = $elemento['idAlumno'];
                include_once ('ingresos-alumnos.php');
              
                  
              ?>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
                Cancelar
              </button>
              <a class="btn btn-primary">Aceptar</a>
            </div>
          </div>        
        </div>
      </div>
    <?php } ?>
=======

       

>>>>>>> 22a457ee132936b0bceedc5eb060949422f5decd
      <!-- Footer -->

      <?php include_once ('../elementos/footer-admin.php');   ?>
     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../../vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/chart-area-demo.js"></script>
  <script src="../../js/demo/chart-pie-demo.js"></script>

   <!-- Page level plugins -->
  <script src="../../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/datatables-demo.js"></script>

  <script>
    $(document).ready(function() {
        $('#dataTableA').DataTable({
            responsive: true,
            language: {
                url: '../../vendor/datatables/es-ar.json' //Ubicacion del archivo con el json del idioma.
            }
        });
    });
 </script>


  <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="../../source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="../../source/jquery.fancybox.css?v=2.1.5" media="screen" />

    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="../../source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <script type="text/javascript" src="../../source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="../../source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="../../source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="../../source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

    <script type="text/javascript">
        $(document).ready(function() {
                        
            $('.fancybox').fancybox();

            $(".fancybox-effects-a").fancybox({
                helpers: {
                    title : {
                        type : 'outside'
                    },
                    overlay : {
                        speedOut : 0
                    }
                }
            });

            
            $(".fancybox-effects-b").fancybox({
                openEffect  : 'none',
                closeEffect : 'none',

                helpers : {
                    title : {
                        type : 'over'
                    }
                }
            });

            
            $("#fancybox-manual-b").click(function() {
                $.fancybox.open({
                    href : 'registro.php',
                    type : 'iframe',
                    padding : 5,
                    
                });
            });

        });
    </script>

</body>

</html>
