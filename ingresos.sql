-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 22-01-2020 a las 19:28:36
-- Versión del servidor: 5.7.28
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ingresos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

DROP TABLE IF EXISTS `alumno`;
CREATE TABLE IF NOT EXISTS `alumno` (
  `idAlumno` int(200) NOT NULL AUTO_INCREMENT,
  `nomalumno` varchar(100) NOT NULL,
  `edad` int(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(200) NOT NULL,
  `idUnidad` int(100) NOT NULL,
  `fecha_nac` date NOT NULL,
  `lugar_nac` varchar(30) NOT NULL,
  `genero` varchar(10) NOT NULL,
  `tipo_sangre` varchar(100) DEFAULT NULL,
  `idEstatus` int(100) NOT NULL,
  PRIMARY KEY (`idAlumno`),
  KEY `Id_UD` (`idUnidad`),
  KEY `Id_estatus` (`idEstatus`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`idAlumno`, `nomalumno`, `edad`, `direccion`, `telefono`, `idUnidad`, `fecha_nac`, `lugar_nac`, `genero`, `tipo_sangre`, `idEstatus`) VALUES
(11, 'alfredo de jesus', 40, 'calle 60 x 50', '9999999999', 2, '2020-01-22', 'MERIDA YUCATAN', 'M', '-O', 5),
(9, 'Alejandro', 24, 'calle 60 x 50', '9889673090', 3, '2020-01-08', 'MERIDA YUCATAN', 'H', '-O', 5),
(10, 'Alfredo', 19, 'calle 60 x 55', '9999999999', 3, '2020-01-13', 'MERIDA YUCATAN', 'M', '+A', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clases`
--

DROP TABLE IF EXISTS `clases`;
CREATE TABLE IF NOT EXISTS `clases` (
  `idClase` int(200) NOT NULL AUTO_INCREMENT,
  `nombre_clase` varchar(100) NOT NULL,
  `profesor` varchar(50) DEFAULT NULL,
  `horario` varchar(50) DEFAULT NULL,
  `idUnidad` int(100) NOT NULL,
  `costo` varchar(10) NOT NULL,
  PRIMARY KEY (`idClase`),
  KEY `Id_UD` (`idUnidad`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clases`
--

INSERT INTO `clases` (`idClase`, `nombre_clase`, `profesor`, `horario`, `idUnidad`, `costo`) VALUES
(2, 'futbol', 'alfredo', '7am- 8:pm', 3, '44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuento`
--

DROP TABLE IF EXISTS `descuento`;
CREATE TABLE IF NOT EXISTS `descuento` (
  `idDescuento` int(10) NOT NULL AUTO_INCREMENT,
  `nombre_descuento` varchar(20) NOT NULL,
  `cantidad` varchar(10) NOT NULL,
  PRIMARY KEY (`idDescuento`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `descuento`
--

INSERT INTO `descuento` (`idDescuento`, `nombre_descuento`, `cantidad`) VALUES
(7, 'Ninguno', '0'),
(8, 'Beca', '20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus`
--

DROP TABLE IF EXISTS `estatus`;
CREATE TABLE IF NOT EXISTS `estatus` (
  `idEstatus` int(10) NOT NULL AUTO_INCREMENT,
  `nombre_estatus` varchar(50) NOT NULL,
  `motivo` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idEstatus`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estatus`
--

INSERT INTO `estatus` (`idEstatus`, `nombre_estatus`, `motivo`) VALUES
(5, 'activo', NULL),
(6, 'activo', NULL),
(7, 'inactivo', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingreso`
--

DROP TABLE IF EXISTS `ingreso`;
CREATE TABLE IF NOT EXISTS `ingreso` (
  `idIngreso` int(200) NOT NULL AUTO_INCREMENT,
  `idAlumno` int(200) NOT NULL,
  `idTipo` int(100) NOT NULL,
  `comentario` varchar(100) DEFAULT NULL,
  `fecha_registro` date NOT NULL,
  `idUnidad` int(100) NOT NULL,
  `idClase` int(100) NOT NULL,
  `mes` varchar(10) DEFAULT NULL,
  `idDescuento` int(10) NOT NULL,
  `importe` varchar(100) NOT NULL,
  `estado_cobro` varchar(100) NOT NULL,
  PRIMARY KEY (`idIngreso`),
  KEY `Id_alumno` (`idAlumno`),
  KEY `Id_UD` (`idUnidad`),
  KEY `Id_clase` (`idClase`),
  KEY `Id_desc` (`idDescuento`),
  KEY `idTipo` (`idTipo`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ingreso`
--

INSERT INTO `ingreso` (`idIngreso`, `idAlumno`, `idTipo`, `comentario`, `fecha_registro`, `idUnidad`, `idClase`, `mes`, `idDescuento`, `importe`, `estado_cobro`) VALUES
(8, 10, 5, 'wwww', '2020-01-21', 3, 2, 'Octubre', 3, '500', 'Pagado'),
(7, 10, 5, 'aaaa', '2020-01-21', 3, 2, 'Diciembre', 3, '100', 'Pagado'),
(6, 9, 3, 'ppppppppppp', '2020-01-21', 3, 2, 'Noviembre', 3, '2', 'Pagado'),
(10, 9, 5, 'no pagó completo', '2020-01-22', 3, 2, 'Noviembre', 5, '600', 'Pagado'),
(11, 9, 5, 'no pagó completo', '2020-01-22', 3, 2, 'Noviembre', 5, '600', 'Pagado'),
(12, 10, 5, 'sssssss', '2020-01-22', 3, 2, 'Julio', 3, '1', 'Cancelado'),
(13, 11, 3, 'aaaa', '2020-01-22', 2, 2, '', 8, '500', 'Pagado'),
(14, 11, 5, 'sssssss', '2020-01-22', 3, 2, 'Diciembre', 7, '2', 'Pagado'),
(15, 11, 5, 'aaaa', '2020-01-22', 2, 2, 'Diciembre', 7, '100', 'Pagado'),
(16, 11, 3, 'aaaa', '2020-01-22', 2, 2, NULL, 7, '100', 'Pagado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

DROP TABLE IF EXISTS `permiso`;
CREATE TABLE IF NOT EXISTS `permiso` (
  `idPermiso` int(11) NOT NULL AUTO_INCREMENT,
  `nomPermiso` varchar(200) NOT NULL,
  `clave` varchar(50) NOT NULL,
  PRIMARY KEY (`idPermiso`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`idPermiso`, `nomPermiso`, `clave`) VALUES
(3, 'ADMINISTRADOR', 'ADMIN'),
(2, 'USUARIO', 'USER'),
(9, 'SUPER ADMINISTRADOR', 'SUPADMIN'),
(10, 'prueba', 'prtub');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

DROP TABLE IF EXISTS `rol`;
CREATE TABLE IF NOT EXISTS `rol` (
  `idRol` int(11) NOT NULL AUTO_INCREMENT,
  `nomRol` varchar(100) NOT NULL,
  PRIMARY KEY (`idRol`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idRol`, `nomRol`) VALUES
(3, 'USUARIO'),
(2, 'ADMINISTRADOR'),
(24, 'SUPER ADMINISTRADOR'),
(25, 'dsfsdfsd'),
(26, 'fggsdffdsfd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolxpermiso`
--

DROP TABLE IF EXISTS `rolxpermiso`;
CREATE TABLE IF NOT EXISTS `rolxpermiso` (
  `idRolxpermiso` int(11) NOT NULL AUTO_INCREMENT,
  `idRol` int(11) NOT NULL,
  `idPermiso` int(11) NOT NULL,
  PRIMARY KEY (`idRolxpermiso`),
  KEY `idRol` (`idRol`),
  KEY `idPermiso` (`idPermiso`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rolxpermiso`
--

INSERT INTO `rolxpermiso` (`idRolxpermiso`, `idRol`, `idPermiso`) VALUES
(30, 2, 2),
(29, 2, 3),
(19, 4, 2),
(18, 4, 3),
(28, 3, 2),
(23, 24, 3),
(24, 24, 2),
(25, 24, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_ingreso`
--

DROP TABLE IF EXISTS `tipo_ingreso`;
CREATE TABLE IF NOT EXISTS `tipo_ingreso` (
  `idTipo` int(100) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`idTipo`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_ingreso`
--

INSERT INTO `tipo_ingreso` (`idTipo`, `nombre`) VALUES
(5, 'Mensualidad'),
(3, 'inscripcion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad`
--

DROP TABLE IF EXISTS `unidad`;
CREATE TABLE IF NOT EXISTS `unidad` (
  `idUnidad` int(200) NOT NULL AUTO_INCREMENT,
  `nombre_unidad` varchar(100) NOT NULL,
  `direccion` varchar(20) NOT NULL,
  `serie` varchar(10) NOT NULL,
  PRIMARY KEY (`idUnidad`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `unidad`
--

INSERT INTO `unidad` (`idUnidad`, `nombre_unidad`, `direccion`, `serie`) VALUES
(3, 'IDEY', 'calle 60 x 55', 'D3'),
(2, 'kukulcan', 'calle 60 x 50', 'Z3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nomUsuario` varchar(200) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pasword` varchar(255) NOT NULL,
  `departamento` varchar(250) NOT NULL,
  `idRol` int(11) NOT NULL,
  PRIMARY KEY (`idUsuario`),
  KEY `idRol` (`idRol`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `nomUsuario`, `email`, `pasword`, `departamento`, `idRol`) VALUES
(15, 'Alfredo Carmona Irigoyen', 'alfredo.carmona', '827ccb0eea8a706c4c34a16891f84e7b', 'Sistemas Informaticos', 2),
(16, 'Usuario Prueba', 'usuario.prueba', '202cb962ac59075b964b07152d234b70', 'Unidad deportida del sur', 3),
(12, 'Super Administrador', 'sup-admin', '0c83f64e778d04545cdcab47a041bc2c', 'Sistemas de Informacion', 24),
(17, 'Prueba', 'prueba.usuario', '202cb962ac59075b964b07152d234b70', 'Sistemas', 3);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
