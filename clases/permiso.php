<?php
include_once('conexion.php');
class permiso{
	var $idPermiso;
	var $nomPermiso;
    var $clave;
	
	
	function __construct(){
		$this->idPermiso=0;
		$this->nomPermiso='';
        $this->clave='';
	}

    function insertar_permiso(){


    	$conexion=new Conexion();
    	$consulta='insert into permiso(nomPermiso,
                                        clave)
                              values(:nomPermiso,
                                     :clave)';

$datos=array(
	':nomPermiso'=>$this->nomPermiso,
    ':clave'=>$this->clave,
	         );
       $conexion->ejecutar_consulta($consulta,$datos);
    }

    function modificar_permiso(){
    	$conexion=new Conexion();
		$consulta='update permiso set 
		nomPermiso=:nomPermiso,
        clave=:clave
		where idPermiso=:idPermiso';

   $datos=array(
   	             ':nomPermiso'=>$this->nomPermiso,
                 ':clave'=>$this->clave,
   	             ':idPermiso'=>$this->idPermiso,             
   	);
   $conexion->ejecutar_consulta($consulta,$datos);
    }

    function eliminar_permiso(){
    	$conexion=new Conexion();
    	$consulta='delete from permiso where idPermiso=:idPermiso';

    	$datos=array(
                      ':idPermiso'=>$this->idPermiso,
    	);
    	$conexion->ejecutar_consulta($consulta,$datos);
    }
    function obtener_permiso(){
    	$conexion=new Conexion();
    	$consulta='select idPermiso,
    	                   nomPermiso,
                           clave
    	   
    	                   from permiso
    	                   where idPermiso=:idPermiso';

     $datos=array(
     	           ':idPermiso'=>$this->idPermiso,
     	);

     $resultados=$conexion->ejecutar_consulta($consulta,$datos);
     $resultados->setFetchMode(PDO::FETCH_INTO,$this);
     $resultados->fetch();
    }

    function listar_permiso(){
    	$lista_permisos=array();
    	$conexion=new Conexion();
    	$consulta='select idPermiso,
    	                   nomPermiso,
                           clave
    	                   
    	                   from permiso';
    	$datos=array();
    	$resultados=$conexion->ejecutar_consulta($consulta,$datos);
    	$resultados->setFetchMode(PDO::FETCH_ASSOC);
    	$lista_permisos=$resultados->fetchAll();
    	return $lista_permisos;
    }
}
?>