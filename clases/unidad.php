<?php
include_once('conexion.php');
class unidad{
	var $idUnidad;
	var $nombre_unidad;
	var $direccion;
	var $serie;
	
	
	function __construct(){
		$this->idUnidad=0;
		$this->nombre_unidad='';
		$this->direccion='';
		$this->serie='';
	}

    function insertar_unidad(){


    	$conexion=new Conexion();
    	$consulta='insert into unidad(nombre_unidad,
										direccion,
										serie)
                              values(:nombre_unidad,
									 :direccion,
									 :serie)';

$datos=array(
	':nombre_unidad'=>$this->nombre_unidad,
	':direccion'=>$this->direccion,
	':serie'=>$this->serie,
	         );
       $conexion->ejecutar_consulta($consulta,$datos);
    }

    function modificar_unidad(){
    	$conexion=new Conexion();
		$consulta='update unidad set 
		nombre_unidad=:nombre_unidad,
		direccion=:direccion,
		serie=:serie
		where idUnidad=:idUnidad';

   $datos=array(
   	             ':nombre_unidad'=>$this->nombre_unidad,
				 ':direccion'=>$this->direccion,
				 ':serie'=>$this->serie,
   	             ':idUnidad'=>$this->idUnidad,             
   	);
   $conexion->ejecutar_consulta($consulta,$datos);
    }

    function eliminar_unidad(){
    	$conexion=new Conexion();
    	$consulta='delete from unidad where idUnidad=:idUnidad';

    	$datos=array(
                      ':idUnidad'=>$this->idUnidad,
    	);
    	$conexion->ejecutar_consulta($consulta,$datos);
    }
    function obtener_unidad(){
    	$conexion=new Conexion();
    	$consulta='select idUnidad,
    	                   nombre_unidad,
						   direccion,
						   serie
    	   
    	                   from unidad
    	                   where idUnidad=:idUnidad';

     $datos=array(
     	           ':idUnidad'=>$this->idUnidad,
     	);

     $resultados=$conexion->ejecutar_consulta($consulta,$datos);
     $resultados->setFetchMode(PDO::FETCH_INTO,$this);
     $resultados->fetch();
    }

    function listar_unidad(){
    	$lista_unidad=array();
    	$conexion=new Conexion();
    	$consulta='select idUnidad,
						   nombre_unidad,
						   direccion,
                           serie
    	                   
    	                   from unidad';
    	$datos=array();
    	$resultados=$conexion->ejecutar_consulta($consulta,$datos);
    	$resultados->setFetchMode(PDO::FETCH_ASSOC);
    	$lista_unidad=$resultados->fetchAll();
    	return $lista_unidad;
    }
}
?>