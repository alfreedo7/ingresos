<?php
include_once('conexion.php');
class tipo{
	var $idTipo;
	var $nombre;
	
	function __construct(){
		$this->idTipo=0;
		$this->nombre='';
	}

    function insertar_tipo(){


    	$conexion=new Conexion();
    	$consulta='insert into tipo_ingreso(nombre
										)
							  values(:nombre
							  			)';

$datos=array(
	':nombre'=>$this->nombre,
	         );
       $conexion->ejecutar_consulta($consulta,$datos);
    }

    function modificar_tipo(){
    	$conexion=new Conexion();
		$consulta='update tipo_ingreso set 
		nombre=:nombre where idTipo=:idTipo';

   $datos=array(
					':nombre'=>$this->nombre,
					':idTipo'=>$this->idTipo,            
   	);
   $conexion->ejecutar_consulta($consulta,$datos);
    }

    function eliminar_tipo(){
    	$conexion=new Conexion();
    	$consulta='delete from tipo_ingreso where idTipo=:idTipo';

    	$datos=array(
                      ':idTipo'=>$this->idTipo,
    	);
    	$conexion->ejecutar_consulta($consulta,$datos);
    }
    function obtener_tipo(){
    	$conexion=new Conexion();
    	$consulta='select idTipo,
    	                   nombre
    	   
    	                   from tipo_ingreso
    	                   where idTipo=:idTipo';

     $datos=array(
     	           ':idTipo'=>$this->idTipo,
     	);

     $resultados=$conexion->ejecutar_consulta($consulta,$datos);
     $resultados->setFetchMode(PDO::FETCH_INTO,$this);
     $resultados->fetch();
    }

    function listar_tipo(){
    	$lista_tipo=array();
    	$conexion=new Conexion();
    	$consulta='select idTipo,
						   nombre
    	                   
    	                   from tipo_ingreso';
    	$datos=array();
    	$resultados=$conexion->ejecutar_consulta($consulta,$datos);
    	$resultados->setFetchMode(PDO::FETCH_ASSOC);
    	$lista_tipo=$resultados->fetchAll();
    	return $lista_tipo;
    }
}
?>