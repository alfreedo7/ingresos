<?php
include_once('conexion.php');
class estatus{
	var $idEstatus;
	var $nombre_estatus;	
	
	function __construct(){
		$this->idEstatus=0;
		$this->nombre_estatus='';
	}

    function insertar_estatus(){


    	$conexion=new Conexion();
		$consulta='insert into estatus(nombre_estatus
										)
							  values(:nombre_estatus
							  )';

$datos=array(
	':nombre_estatus'=>$this->nombre_estatus,
	         );
       $conexion->ejecutar_consulta($consulta,$datos);
    }

    function modificar_estatus(){
    	$conexion=new Conexion();
		$consulta='update estatus set 
		nombre_estatus=:nombre_estatus
		where idEstatus=:idEstatus';

   $datos=array(
   	             ':nombre_estatus'=>$this->nombre_estatus,
   	             ':idEstatus'=>$this->idEstatus,             
   	);
   $conexion->ejecutar_consulta($consulta,$datos);
    }

    function eliminar_estatus(){
    	$conexion=new Conexion();
    	$consulta='delete from estatus where idEstatus=:idEstatus';

    	$datos=array(
                      ':idEstatus'=>$this->idEstatus,
    	);
    	$conexion->ejecutar_consulta($consulta,$datos);
    }
    function obtener_estatus(){
    	$conexion=new Conexion();
    	$consulta='select idEstatus,
    	                   nombre_estatus
    	   
    	                   from estatus
    	                   where idEstatus=:idEstatus';

     $datos=array(
     	           ':idEstatus'=>$this->idEstatus,
     	);

     $resultados=$conexion->ejecutar_consulta($consulta,$datos);
     $resultados->setFetchMode(PDO::FETCH_INTO,$this);
     $resultados->fetch();
    }

    function listar_estatus(){
    	$lista_estatus=array();
    	$conexion=new Conexion();
    	$consulta='select idEstatus,
    	                   nombre_estatus
    	                   
    	                   from estatus';
    	$datos=array();
    	$resultados=$conexion->ejecutar_consulta($consulta,$datos);
    	$resultados->setFetchMode(PDO::FETCH_ASSOC);
    	$lista_estatus=$resultados->fetchAll();
    	return $lista_estatus;
    }
}
?>