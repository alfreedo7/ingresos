<?php
include_once('conexion.php');
include_once('unidad.php');
class clases{
    var $idClase;
	  var $nombre_clase;
	  var $profesor;
    var $horario;
    var $costo;
    var $idUnidad;

	
	function __construct(){
		$this->idClase=0;
        $this->nombre_clase='';
		$this->profesor='';
    $this->horario='';
    $this->costo='';

   
    
        $this->idUnidad='';
	}

    function insertar_clases(){


    	$conexion=new Conexion();
    	$consulta='insert into clases(
                                    nombre_clase,
                                     profesor,
                                   horario,
                                   costo,
                                   
                                   
                                       idUnidad
                                       )
                              values(:nombre_clase,
                                     :profesor,
                                    
                                     :horario,
                                     :costo,
                                     
                                     
                                     :idUnidad
                                     )';

$datos=array(
  ':nombre_clase'=>$this->nombre_clase,
	':profesor'=>$this->profesor,
  ':horario'=>$this->horario,
  ':costo'=>$this->costo,
  
  
    ':idUnidad'=>$this->idUnidad,
	         );
       $conexion->ejecutar_consulta($consulta,$datos);
    }

    function modificar_clases(){
    	$conexion=new Conexion();
        $datos=array(
                 ':nombre_clase'=>$this->nombre_clase,
                 ':profesor'=>$this->profesor,
                 ':idClase'=>$this->idClase,
                 ':horario'=>$this->horario,
                   ':costo'=>$this->costo,

                 
                 
                 ':idUnidad'=>$this->idUnidad,             
                    );

    	$consulta='update clases set nombre_clase=:nombre_clase, profesor=:profesor, horario=:horario, costo=:costo, idUnidad=:idUnidad    	                             
    	                             where idClase=:idClase';

   
   $conexion->ejecutar_consulta($consulta,$datos);
    }

    function eliminar_clases(){
      $conexion=new Conexion();
      $consulta='delete from clases where idClase=:idClase';

      $datos=array(
                      ':idClase'=>$this->idClase,
      );
      $conexion->ejecutar_consulta($consulta,$datos);
    }


    function obtener_clases(){
        $conexion=new Conexion();
        $consulta='select idClase,
                           nombre_clase,
                           profesor,
                           horario,
                           costo,
                           
                           
                           idUnidad
                           from clases
                           where idClase=:idClase';

     $datos=array(
                   ':idClase'=>$this->idClase,
        );

     $resultados=$conexion->ejecutar_consulta($consulta,$datos);
     $resultados->setFetchMode(PDO::FETCH_INTO,$this);
     $resultados->fetch();
    }

    function listar_clases(){
        $lista_clase=array();
        $conexion=new Conexion();
        $consulta='select *from clases';
        $datos=array();
        $resultados=$conexion->ejecutar_consulta($consulta,$datos);
        $resultados->setFetchMode(PDO::FETCH_ASSOC);
        $lista_clase=$resultados->fetchAll();
        return $lista_clase;
    }

     



    function obtener_unidad(){
        $listado=new listado();
        $listado->idUnidad=$this->idUnidad;
        $listado->obtener_unidad();
        return $listado;
    }

    function listar_clases_unidad(){
      $lista_usuarios=array();
      $conexion=new Conexion();
      $consulta='select *from clases where idUnidad=:id';
      $datos=array(':id'=>$this->idUnidad);
      $resultados=$conexion->ejecutar_consulta($consulta,$datos);
      $resultados->setFetchMode(PDO::FETCH_ASSOC);
      $lista_usuarios=$resultados->fetchAll();
      return $lista_usuarios;
    }


}
?>