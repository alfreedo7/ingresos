<?php
include_once('conexion.php');
include_once('rol.php');
class usuario{
    var $idUsuario;
	  var $nomUsuario;
	  var $email;
	  var $pasword;
    var $departamento;
    var $idRol;

	
	function __construct(){
		$this->idUsuario=0;
        $this->nomUsuario='';
		$this->email='';
		$this->pasword='';
    $this->departamento='';
   
    
        $this->idRol='';
	}

    function insertar_usuario(){


    	$conexion=new Conexion();
    	$consulta='insert into usuario(
                                    nomUsuario,
                                     email, 
    		                           pasword,
                                   departamento,
                                   
                                   
                                       idRol
                                       )
                              values(:nomUsuario,
                                     :email,
                                     MD5(:pasword),
                                     :departamento,
                                     
                                     
                                     :idRol
                                     )';

$datos=array(
  ':nomUsuario'=>$this->nomUsuario,
	':email'=>$this->email,
	':pasword'=>$this->pasword,
  ':departamento'=>$this->departamento,
  
  
    ':idRol'=>$this->idRol,
	         );
       $conexion->ejecutar_consulta($consulta,$datos);
    }

    function modificar_usuario(){
    	$conexion=new Conexion();
        $datos=array(
                 ':nomUsuario'=>$this->nomUsuario,
                 ':email'=>$this->email,
                 ':idUsuario'=>$this->idUsuario,
                 ':departamento'=>$this->departamento,

                 
                 
                 ':idRol'=>$this->idRol,             
                    );
        $pedazo='';
        if($this->pasword!=''){
            $datos[':pasword']=$this->pasword;
            $pedazo=',pasword=MD5(:pasword)';
        }
    	$consulta='update usuario set nomUsuario=:nomUsuario, email=:email, departamento=:departamento,  idRol=:idRol'.$pedazo.'    	                             
    	                             where idUsuario=:idUsuario';

   
   $conexion->ejecutar_consulta($consulta,$datos);
    }

    function eliminar_usuario(){
      $conexion=new Conexion();
      $consulta='delete from usuario where idUsuario=:idUsuario';

      $datos=array(
                      ':idUsuario'=>$this->idUsuario,
      );
      $conexion->ejecutar_consulta($consulta,$datos);
    }


    function obtener_usuario(){
        $conexion=new Conexion();
        $consulta='select idUsuario,
                           nomUsuario,
                           email,
                           pasword,
                           departamento,
                           
                           
                           idRol
                           from usuario
                           where idUsuario=:idUsuario';

     $datos=array(
                   ':idUsuario'=>$this->idUsuario,
        );

     $resultados=$conexion->ejecutar_consulta($consulta,$datos);
     $resultados->setFetchMode(PDO::FETCH_INTO,$this);
     $resultados->fetch();
    }

    function listar_usuario(){
        $lista_usuarios=array();
        $conexion=new Conexion();
        $consulta='select *from usuario';
        $datos=array();
        $resultados=$conexion->ejecutar_consulta($consulta,$datos);
        $resultados->setFetchMode(PDO::FETCH_ASSOC);
        $lista_usuarios=$resultados->fetchAll();
        return $lista_usuarios;
    }

    function listar_ticket_usuario(){
      $lista_usuarios=array();
      $conexion=new Conexion();
      $consulta='select *from ticket where idUsuario=:id';
      $datos=array(':id'=>$this->idUsuario);
      $resultados=$conexion->ejecutar_consulta($consulta,$datos);
      $resultados->setFetchMode(PDO::FETCH_ASSOC);
      $lista_usuarios=$resultados->fetchAll();
      return $lista_usuarios;
    }



    function obtener_rol(){
        $rol=new rol();
        $rol->idRol=$this->idRol;
        $rol->obtener_rol();
        return $rol;
    }

    function login(){
    	$conexion=new Conexion();
        $bandera=0;
    	$consulta='select * from usuario
                    where   email=:email
                    and     pasword=MD5(:pasword)';

    	$datos=array(
                      ':email'=>$this->email,
                      ':pasword'=>$this->pasword,
    	             );
    	$resultados=$conexion->ejecutar_consulta($consulta,$datos);
    	$resultados->setFetchMode(PDO::FETCH_INTO,$this);
    	$resultados->fetch();
    	if($this->idUsuario!=0){
            $bandera=1;
        }
        return $bandera;
    }
}
?>