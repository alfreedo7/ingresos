<?php
include_once('conexion.php');
class rolxpermiso{
	var $idRol;
	var $idPermiso;
	
	
	function __construct(){
		$this->idRol=0;
		$this->idPermiso=0;		
	}

    function insertar_rolxpermiso(){


    	$conexion=new Conexion();
    	$consulta='insert into rolxpermiso(idRol,
    		                           idPermiso)
                              values(:idRol,
                                     :idPermiso)';

$datos=array(
	':idRol'=>$this->idRol,
	':idPermiso'=>$this->idPermiso,
	         );
       $conexion->ejecutar_consulta($consulta,$datos);
    }



    function desasignar_permiso(){
    	$conexion=new Conexion();
    	$consulta='delete from rolxpermiso where idRol=:idRol';

    	$datos=array(
                      ':idRol'=>$this->idRol,
    	);
    	$conexion->ejecutar_consulta($consulta,$datos);
    }

    function validar_rolxpermiso(){
    	$conexion=new Conexion();
    	$consulta='select count(*) as total  	                   
    	                   from rolxpermiso
    	                   where idRol=:idRol
                           and idPermiso=:idPermiso';

     $datos=array(
     	           ':idRol'=>$this->idRol,
                   ':idPermiso'=>$this->idPermiso,
     	);
     $resultados=$conexion->ejecutar_consulta($consulta,$datos);
     $resultados->setFetchMode(PDO::FETCH_INTO,$this);
     $resultados->fetch();
     $bandera=0;
     if($this->total!=0)
        $bandera=1;
        return $bandera;

    }  

    function validar_rolxpermiso_usuario($UsrID,$cvepermiso){
    	$conexion=new Conexion();
    	$consulta='select count(*) as total  	                   
    	                   from usuario,
                                rol,
                                rolxpermiso,
                                permiso
    	                   where usuario.idRol=rol.idRol
                           and rol.idRol=rolxpermiso.idRol
                           and rolxpermiso.idPermiso=permiso.idPermiso
                           and usuario.idUsuario=:idUsuario
                           and clave=:clave';

     $datos=array(
     	           ':idUsuario'=>$UsrID,
                   ':clave'=>$cvepermiso,
     	);
     $resultados=$conexion->ejecutar_consulta($consulta,$datos);
     $resultados->setFetchMode(PDO::FETCH_INTO,$this);
     $resultados->fetch();
     $bandera=0;
     if($this->total!=0)
        $bandera=1;
        return $bandera;

    }      
}
?>