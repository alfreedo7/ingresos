<?php
include_once('conexion.php');
class descuento{
	var $idDescuento;
	var $nombre_descuento;
    var $cantidad;
	
	
	function __construct(){
		$this->idDescuento=0;
		$this->nombre_descuento='';
        $this->cantidad='';
	}

    function insertar_descuento(){


    	$conexion=new Conexion();
    	$consulta='insert into descuento(nombre_descuento,
                                        cantidad)
                              values(:nombre_descuento,
                                     :cantidad)';

$datos=array(
	':nombre_descuento'=>$this->nombre_descuento,
    ':cantidad'=>$this->cantidad,
	         );
       $conexion->ejecutar_consulta($consulta,$datos);
    }

    function modificar_descuento(){
    	$conexion=new Conexion();
		$consulta='update descuento set 
		nombre_descuento=:nombre_descuento,
        cantidad=:cantidad
		where idDescuento=:idDescuento';

   $datos=array(
   	             ':nombre_descuento'=>$this->nombre_descuento,
                 ':cantidad'=>$this->cantidad,
   	             ':idDescuento'=>$this->idDescuento,             
   	);
   $conexion->ejecutar_consulta($consulta,$datos);
    }

    function eliminar_descuento(){
    	$conexion=new Conexion();
    	$consulta='delete from descuento where idDescuento=:idDescuento';

    	$datos=array(
                      ':idDescuento'=>$this->idDescuento,
    	);
    	$conexion->ejecutar_consulta($consulta,$datos);
    }
    function obtener_descuento(){
    	$conexion=new Conexion();
    	$consulta='select idDescuento,
    	                   nombre_descuento,
                           cantidad
    	   
    	                   from descuento
    	                   where idDescuento=:idDescuento';

     $datos=array(
     	           ':idDescuento'=>$this->idDescuento,
     	);

     $resultados=$conexion->ejecutar_consulta($consulta,$datos);
     $resultados->setFetchMode(PDO::FETCH_INTO,$this);
     $resultados->fetch();
    }

    function listar_descuento(){
    	$lista_descuento=array();
    	$conexion=new Conexion();
    	$consulta='select idDescuento,
    	                   nombre_descuento,
                           cantidad
    	                   
    	                   from descuento';
    	$datos=array();
    	$resultados=$conexion->ejecutar_consulta($consulta,$datos);
    	$resultados->setFetchMode(PDO::FETCH_ASSOC);
    	$lista_descuento=$resultados->fetchAll();
    	return $lista_descuento;
    }
}
?>