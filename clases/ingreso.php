<?php
include_once('conexion.php');
include_once('alumno.php');
include_once('unidad.php');
include_once('clases.php');
include_once('tipo.php');
include_once('descuento.php');

class ingreso{
	var $idIngreso;
	var $idAlumno;
    var $idTipo;
	var $comentario;
	var $fecha_registro;
    var $idUnidad;
    var $idClase;
	var $mes;
    var $idDescuento;
    var $importe;
    var $estado_cobro;

	function __construct(){
		$this->idIngreso=0;
		$this->idAlumno='';
		$this->idTipo='';
		$this->comentario='';
		$this->fecha_registro='';
		$this->idUnidad='';
		$this->idClase='';
		$this->mes='';
		$this->idDescuento='';
		$this->importe='';
		$this->estado_cobro='';

	}

    function insertar_ingreso(){


    	$conexion=new Conexion();
    	$consulta='insert into ingreso(idAlumno,
										idTipo,
										comentario,
										fecha_registro,
										idUnidad,
										idClase,
										mes,
										idDescuento,
										importe,
										estado_cobro
										)
                              values(:idAlumno,
									 :idTipo,
									 :comentario,
									 :fecha_registro,
									 :idUnidad,
									 :idClase,
									 :mes,
									 :idDescuento,
									 :importe,
									 :estado_cobro
									 )';

$datos=array(
	':idAlumno'=>$this->idAlumno,
	':idTipo'=>$this->idTipo,
	':comentario'=>$this->comentario,
	':fecha_registro'=>$this->fecha_registro,
	':idUnidad'=>$this->idUnidad,
	':idClase'=>$this->idClase,
	':mes'=>$this->mes,
	':idDescuento'=>$this->idDescuento,
	':importe'=>$this->importe,
	':estado_cobro'=>$this->estado_cobro,
	         );
       $conexion->ejecutar_consulta($consulta,$datos);
    }

    function modificar_ingreso(){
    	$conexion=new Conexion();
		$consulta='update ingreso set
						idIngreso=:idIngreso,
						idAlumno=:idAlumno,
						idTipo=:idTipo,
						comentario=:comentario,
						fecha_registro=:fecha_registro,
						idUnidad=:idUnidad,
						idClase=:idClase,
						mes=:mes,
						idDescuento=:idDescuento,
						importe=:importe,
						estado_cobro=:estado_cobro

						where idIngreso=:idIngreso';
		
		$datos=array(
				':idIngreso'=>$this->idIngreso,
				':idAlumno'=>$this->idAlumno,
				':idTipo'=>$this->idTipo,
				':comentario'=>$this->comentario,
				':fecha_registro'=>$this->fecha_registro,
				':idUnidad'=>$this->idUnidad,
				':idClase'=>$this->idClase,
				':mes'=>$this->mes,
				':idDescuento'=>$this->idDescuento,
				':importe'=>$this->importe,
				':estado_cobro'=>$this->estado_cobro,

		);
   
   $conexion->ejecutar_consulta($consulta,$datos);
    }

    function eliminar_ingreso(){
    	$conexion=new Conexion();
    	$consulta='delete from ingreso where idIngreso=:idIngreso';

    	$datos=array(
                      ':idIngreso'=>$this->idIngreso,
    	);
    	$conexion->ejecutar_consulta($consulta,$datos);
    }
    function obtener_ingreso(){
    	$conexion=new Conexion();
    	$consulta='select idIngreso,
						   idAlumno,
						   idTipo,
						   comentario,
						   fecha_registro,
						   idUnidad,
						   idClase,
						   mes,
						   idDescuento,
						   importe,
						   estado_cobro
    	   
    	                   from ingreso
    	                   where idIngreso=:idIngreso';

     $datos=array(
     	           ':idIngreso'=>$this->idIngreso,
     	);

     $resultados=$conexion->ejecutar_consulta($consulta,$datos);
     $resultados->setFetchMode(PDO::FETCH_INTO,$this);
     $resultados->fetch();
    }

    function listar_ingreso(){
    	$lista_ingreso=array();
    	$conexion=new Conexion();
    	$consulta='select *from ingreso';
		
		$datos=array();
    	$resultados=$conexion->ejecutar_consulta($consulta,$datos);
    	$resultados->setFetchMode(PDO::FETCH_ASSOC);
    	$lista_ingreso=$resultados->fetchAll();
    	return $lista_ingreso;
	}


	 function listar_ingreso_alumno(){
      $lista_usuarios=array();
      $conexion=new Conexion();
      $consulta='select *from ingreso where idAlumno=:id';
      $datos=array(':id'=>$this->idAlumno);
      $resultados=$conexion->ejecutar_consulta($consulta,$datos);
      $resultados->setFetchMode(PDO::FETCH_ASSOC);
      $lista_usuarios=$resultados->fetchAll();
      return $lista_usuarios;
    }
    
	function obtener_alumno(){
        $alumno=new alumno();
        $alumno->idAlumno=$this->idAlumno;
        $alumno->obtener_alumno();
		return $alumno;
	}
		
	function obtener_unidad(){
        $unidad=new unidad();
        $unidad->idUnidad=$this->idUnidad;
        $unidad->obtener_unidad();
		return $unidad;
	}

	function obtener_clases(){
        $clases=new clases();
        $clases->idClase=$this->idClase;
        $clases->obtener_clases();
		return $clases;
	}

	function obtener_tipo(){
        $tipo=new tipo();
        $tipo->idTipo=$this->idTipo;
        $tipo->obtener_tipo();
		return $tipo;
	}

	function obtener_descuento(){
        $descuento=new descuento();
        $descuento->idDescuento=$this->idDescuento;
        $descuento->obtener_descuento();
		return $descuento;
	}
		
}
?>