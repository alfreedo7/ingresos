<?php 
include_once('usuario.php');
include_once('rolxpermiso.php');
class seguridad {
	function __construct(){
		return true;
	}

	function login($email, $contrasena, $pagina_bien, $pagina_mal){
		$usuario=new usuario();
		$usuario->email=$email;//cambiarlo por email
		$usuario->pasword=$contrasena;
		if ($usuario->login()==1){
			session_start();
			$_SESSION['idUsuario']=$usuario->idUsuario;
			header('location:'.$pagina_bien);
		}
		else
		{
			//header('location:'.$pagina_mal);
			header('location:../../login.php?fallo=true');
		}
	}

	function candado($pagina_mal){
		if(!isset($_SESSION['idUsuario'])){
			header('location:'.$pagina_mal);
			exit();
		}
	}

	function candado_permiso($usuario,$cvepermiso){
		$rxp=new rolxpermiso();
		$bandera=$rxp->validar_rolxpermiso_usuario($usuario,$cvepermiso);
		if($bandera==0)
		{  
			header('location: ../acceso-denegado.php');
			exit();
		}	
	}
	

	function cerrar_sesion($pagina_cerrar){
		session_start();
		session_destroy();
		header('location: '.$pagina_cerrar);
	}





}
 ?>
 
 