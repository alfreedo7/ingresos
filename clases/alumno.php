<?php
include_once('conexion.php');
include_once('unidad.php');
include_once('estatus.php');
class alumno{
    var $idAlumno;
	var $nomalumno;
	var $edad;
	var $direccion;
    var $telefono;
    var $idUnidad;
    var $fecha_nac;
    var $lugar_nac;
    var $genero;
    var $tipo_sangre;
    var $idEstatus;
    var $motivo;

	
	function __construct(){
		$this->idAlumno=0;
        $this->nomalumno='';
		$this->edad='';
		$this->direccion='';
        $this->telefono='';
        $this->idUnidad='';
        $this->fecha_nac='';
        $this->lugar_nac='';
        $this->genero='';
        $this->tipo_sangre='';
        $this->idEstatus='';
        $this->motivo='';

	}

    function insertar_alumno(){


    	$conexion=new Conexion();
    	$consulta='insert into alumno(
                                    nomalumno,
                                    edad, 
    		                        direccion,
                                    telefono,
                                    idUnidad,
                                    fecha_nac,
                                    lugar_nac,
                                    genero,
                                    tipo_sangre,
                                    idEstatus,
                                    motivo
                                       )
                              values(:nomalumno,
                                        :edad, 
                                        :direccion,
                                        :telefono,
                                        :idUnidad,
                                        :fecha_nac,
                                        :lugar_nac,
                                        :genero,
                                        :tipo_sangre,
                                        :idEstatus,
                                        :motivo
                                     )';

$datos=array(
        ':nomalumno'=>$this->nomalumno,
        ':edad'=>$this->edad,
        ':direccion'=>$this->direccion,
        ':telefono'=>$this->telefono,
        ':idUnidad'=>$this->idUnidad,
        ':fecha_nac'=>$this->fecha_nac,
        ':lugar_nac'=>$this->lugar_nac,
        ':genero'=>$this->genero,
        ':tipo_sangre'=>$this->tipo_sangre,
        ':idEstatus'=>$this->idEstatus,
        ':motivo'=>$this->motivo,

	         );
       $conexion->ejecutar_consulta($consulta,$datos);
    }

    function modificar_alumno(){
    	$conexion=new Conexion();
        
        $consulta='update alumno set 
                                    idAlumno=:idAlumno,
                                     nomalumno=:nomalumno,
                                     edad=:edad,
                                     direccion=:direccion,
                                     telefono=:telefono,
                                     idUnidad=:idUnidad,
                                     fecha_nac=:fecha_nac,
                                     lugar_nac=:lugar_nac,
                                     genero=:genero,
                                     tipo_sangre=:tipo_sangre,
                                     idEstatus=:idEstatus,
                                     motivo=:motivo
                                     where idAlumno=:idAlumno
                                     ';
        $datos=array(
                                    'idAlumno'=>$this->idAlumno,
                                    ':nomalumno'=>$this->nomalumno,
                                    ':edad'=>$this->edad,
                                    ':direccion'=>$this->direccion,
                                    ':telefono'=>$this->telefono,
                                    ':idUnidad'=>$this->idUnidad,
                                    ':fecha_nac'=>$this->fecha_nac,
                                    ':lugar_nac'=>$this->lugar_nac,
                                    ':genero'=>$this->genero,
                                    ':tipo_sangre'=>$this->tipo_sangre,
                                    ':idEstatus'=>$this->idEstatus,
                                    ':motivo'=>$this->motivo,
                                               
                                            );
                                
   
   $conexion->ejecutar_consulta($consulta,$datos);
    }

    function eliminar_alumno(){
      $conexion=new Conexion();
      $consulta='delete from alumno where idAlumno=:idAlumno';

      $datos=array(
                      ':idAlumno'=>$this->idAlumno,
      );
      $conexion->ejecutar_consulta($consulta,$datos);
    }


    function obtener_alumno(){
        $conexion=new Conexion();
        $consulta='select idAlumno,
                            nomalumno,
                            edad, 
                            direccion,
                            telefono,
                            idUnidad,
                            fecha_nac,
                            lugar_nac,
                            genero,
                            tipo_sangre,
                            idEstatus,
                            motivo
                            from alumno
                            where idAlumno=:idAlumno';

     $datos=array(
                   ':idAlumno'=>$this->idAlumno,
        );

     $resultados=$conexion->ejecutar_consulta($consulta,$datos);
     $resultados->setFetchMode(PDO::FETCH_INTO,$this);
     $resultados->fetch();
    }

    function listar_alumno(){
        $lista_alumno=array();
        $conexion=new Conexion();
        $consulta='select *from alumno';
        $datos=array();
        $resultados=$conexion->ejecutar_consulta($consulta,$datos);
        $resultados->setFetchMode(PDO::FETCH_ASSOC);
        $lista_alumno=$resultados->fetchAll();
        return $lista_alumno;
    }
    function obtener_unidad(){
        $unidad=new unidad();
        $unidad->idUnidad=$this->idUnidad;
        $unidad->obtener_unidad();
        return $unidad;
    }

    function obtener_estatus(){
        $estatus=new estatus();
        $estatus->idEstatus=$this->idEstatus;
        $estatus->obtener_estatus();
        return $estatus;
    }

    function listar_ingreso_alumno(){
      $lista_usuarios=array();
      $conexion=new Conexion();
      $consulta='select *from ingreso where idAlumno=:id';
      $datos=array(':id'=>$this->idLocalidadp);
      $resultados=$conexion->ejecutar_consulta($consulta,$datos);
      $resultados->setFetchMode(PDO::FETCH_ASSOC);
      $lista_usuarios=$resultados->fetchAll();
      return $lista_usuarios;
    }

    
    function listar_alumno_unidad(){
        $lista_usuarios=array();
        $conexion=new Conexion();
        $consulta='select *from alumno where idUnidad=:id';
        $datos=array(':id'=>$this->idUnidad);
        $resultados=$conexion->ejecutar_consulta($consulta,$datos);
        $resultados->setFetchMode(PDO::FETCH_ASSOC);
        $lista_usuarios=$resultados->fetchAll();
        return $lista_usuarios;
      }

      function listar_alumno_estatus(){
        $lista_usuarios=array();
        $conexion=new Conexion();
        $consulta='select * from alumno where idUnidad=:id';
        $datos=array(':id'=>$this->idUnidad);
        $resultados=$conexion->ejecutar_consulta($consulta,$datos);
        $resultados->setFetchMode(PDO::FETCH_ASSOC);
        $lista_usuarios=$resultados->fetchAll();
        return $lista_usuarios;
      }
    }
?>