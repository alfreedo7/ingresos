<?php
include_once('conexion.php');
class rol{
	var $idRol;
	var $nomRol;	
	
	function __construct(){
		$this->idRol=0;
		$this->nomRol='';
	}

    function insertar_rol(){
    	$conexion=new Conexion();
    	$consulta='insert into rol(nomRol)
                              values(:nomRol)';

    $datos=array(
	':nomRol'=>$this->nomRol,
	         );
       $conexion->ejecutar_consulta($consulta,$datos);
    }

    function modificar_rol(){
    	$conexion=new Conexion();
    	$consulta='update rol set nomRol=:nomRol
    	                             
    	                             where idRol=:idRol';

   $datos=array(
   	             ':nomRol'=>$this->nomRol,
   	             ':idRol'=>$this->idRol,             
   	);
   $conexion->ejecutar_consulta($consulta,$datos);
    }

    function eliminar_rol(){
    	$conexion=new Conexion();
    	$consulta='delete from rol where idRol=:idRol';

    	$datos=array(
                      ':idRol'=>$this->idRol,
    	);
    	$conexion->ejecutar_consulta($consulta,$datos);
    }
    function obtener_rol(){
    	$conexion=new Conexion();
    	$consulta='select *from rol
    	                   where idRol=:idRol';

     $datos=array(
     	           ':idRol'=>$this->idRol,
     	);

     $resultados=$conexion->ejecutar_consulta($consulta,$datos);
     $resultados->setFetchMode(PDO::FETCH_INTO,$this);
     $resultados->fetch();
    }

    function listar_rol(){
    	$lista_roles=array();
    	$conexion=new Conexion();
    	$consulta='select *from rol';
    	$datos=array();
    	$resultados=$conexion->ejecutar_consulta($consulta,$datos);
    	$resultados->setFetchMode(PDO::FETCH_ASSOC);
    	$lista_roles=$resultados->fetchAll();
    	return $lista_roles;
    }
}
?>