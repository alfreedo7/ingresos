<?php
class conexion {
	var $base_datos;
	var $usuario;
	var $password;
	var $host;
	var $pdo;
	var $motor;

	function __construct(){
	$this->base_datos="ingresos";
	$this->usuario="root";
	$this->password="";
	$this->host="localhost";
	$this->motor="mysql";
	}
	function abrir_conexion(){
		$this->pdo=new PDO($this->motor.':host='.$this->host.';dbname='.$this->base_datos.
			';charset=utf8',$this->usuario,$this->password);
		$this->pdo->SetAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	}
	function ejecutar_consulta($consulta,$datos){
		$this->abrir_conexion();
		try{
			$sentencia=$this->pdo->prepare($consulta);
			$sentencia->execute($datos);
			return $sentencia;
		}
		catch(pdoexception $e)
		{
			echo "error:".$e->getmessage().'<br/>';
		}
	}

	function get_lastInserID(){
				
		$LAST_ID = $this->pdo->lastInsertId();	

		return 	$LAST_ID;
	}


	
}


?>