<?php
session_start();
include_once('clases/seguridad.php');
$seg=new seguridad();
$seg->candado('login.php');
$seg->candado_permiso($_SESSION['idUsuario'], 'USER');

if(isset($_POST['logOut'])){
    $seg->cerrar_sesion("../login.php");
}

include_once('clases/usuario.php');
$ObjUser=new usuario();
$ObjUser->idUsuario=$_SESSION['idUsuario'];
$ObjUser->obtener_usuario();


if(isset($_GET['id']))
{
    $operacion='Modificar';
    $configuracion->idUsuario=$_GET['id'];
    $configuracion->obtener_usuario();
    
    $boton_eliminar='<input type="submit" value="Eliminar" name="operacion" class="btn btn-outline-danger">';
}else{
    $operacion='Agregar';
    $boton_eliminar='';
}

?>


<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="shortcut icon" type="image/x-icon" href="img/idey.ico">

  <title>Ingresos</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.css" rel="stylesheet">

  

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="">

    

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->



        <?php  include_once ('elementos/topbar.php');?>
        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

         
          <!-- Content Row -->
          
           

          <!-- Content Row -->
          <div class="row">

            

            <div class="col-lg-12 mb-4">

              <!-- Illustrations -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h3 class="m-0 font-weight-bold text-primary text-center" >MENÚ PRINCIPAL</h3>
                </div>
                <div class="card-body">
                  <div class="text-center">
                  
                      <div class="row" >
                        <div class="col-lg-3 img-iconos">
                        <a href="opciones/alumnos/ver.php"><img src="img/user.svg"></a><br> Alumnos
                        </div> 
                        <div class="col-lg-3 img-iconos">
                        <img src="img/dollar-sign.svg" href=""><br> Realizar pagos
                        </div> 
                        <div class="col-lg-3 img-iconos">
                        <img src="img/calendar.svg" href=""> <br> Agregar clase
                        </div> 
                        <div class="col-lg-3 img-iconos">
                        <img src="img/life-buoy.svg" href=""> <br> Registrar unidad deportiva
                        </div>
                      </div> 
                      <br>
                      <br>
                      <br>

                     
                    
                      <div class="row">
                        <div class="col-lg-4 img-iconos ">
                        <img src="img/percent.svg" href=""> <br> Descuentos
                        </div>
                        <div class="col-lg-4 img-iconos">
                        <img src="img/file.svg" href=""> <br> Reporte de Alumnos
                        </div>
                        <div class="col-lg-4 img-iconos">
                        <img src="img/file-text.svg" href=""> <br> Reporte de Ingresos
                        </div>
                      </div>
                  </div> 
                   
                </div>

                
              </div>

              

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
 

     
     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  

 

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

</body>

</html>
