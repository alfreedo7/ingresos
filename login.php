<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Sistema de Ingresos </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/ideyicoo.ico">

    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- adminpro icon CSS
        ============================================ -->
    <link rel="stylesheet" href="css/adminpro-custon-icon.css">
    <!-- meanmenu icon CSS
        ============================================ -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- mCustomScrollbar CSS
        ============================================ -->
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">

    <!-- normalize CSS
        ============================================ -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- form CSS
        ============================================ -->
    <link rel="stylesheet" href="css/form.css">
    <!-- style CSS
        ============================================ -->
    <link rel="stylesheet" href="css/style.css">
    <!-- responsive CSS
        ============================================ -->
    <link rel="stylesheet" href="css/responsive.css">


    <link rel="stylesheet" href="css/labels.css">

    <style type="text/css">

        #aling{
            width: 800px;
            margin-left: auto;
            margin-right: auto;
            padding: 0px;
        }


        
        @font-face {
        font-family: "Panton-Bold";
        src: url("css/fonts/Panton-Bold.woff") format("opentype");
    }
    @font-face {
        font-family: "Panton-Black";
        src: url("css/fonts/Panton-ExtraBlack.woff") format("opentype");
    }
    @font-face {
        font-family: "Panton-Regular";
        src: url("css/fonts/Panton-Regular.woff") format("opentype");
    }
    @font-face{
        font-family: "Panton-Thin";
        src: url("css/fonts/Panton-Thin.woff") format("opentype");
    }




    </style>







</head>
<body style="font-family:'Panton-Black'; background-color: #dde2e9; align-content: center;">

    

<br>
<br>

  <div class="container">
    <div class="row">
        <div class="col-lg-12">
        <center> 
            <img src="img/inicio.png" style="width:400px;">
        </center>
        </div>
    </div>
      
  </div>
<br>
<br>

  <div class="container" style="align-content: center;">
    <div class="row">
        <div class="col-lg-12">
        <center> 
           <p style="font-family:  'Panton-Black'; color:#242f5d; font-size: 20px;">SISTEMA DE INGRESOS</p>
        </center>
        </div>
    </div>
      
  </div>
    <!-- login Start-->
  <div class="login-form-area mg-t-30 mg-b-40" >
        <div class="container">
            <div class="row">
                <div class="col-lg-3"></div>
                <form id="signupForm1" class="form-login" onsubmit="miFuncion(this)" method="POST" action="admin/seguridad/script_login.php">
                    <div class="col-lg-6">
                        <div class="login-bg">
                            
                            

                            <div class="form-group-inner col-lg-12">
                                                    
                                  <div class="col-lg-12">
                                        <label>Correo/Usuario</label>                                    
                                        <input type="text" style="height:50px;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius: 5px;padding: 0 4px 0 4px; " class="form-control" id="email1" name="email" aria-describedby="emailHelp" required>
                                                       
                                  </div>

                            </div>

                            <div class="form-group-inner col-lg-12">
                                                    
                                  <div class="col-lg-12">
                                        <label>Contraseña</label>                                    
                                        <input type="password" style="height:50px;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius: 5px;padding: 0 4px 0 4px;" class="form-control" id="password1" name="pasword" required>
                                                       
                                  </div>

                            </div>

                             <div class="form-group-inner col-lg-12">
                                                    
                                  

                            </div>
                            <br>

                            <div class="form-group-inner col-lg-12">
                                                    
                                 <div class="col-lg-2">
                                     
                                 </div>

                                   <div class="g-recaptcha col-lg-6" data-sitekey="6LdrfKMUAAAAAERFZK7tgnajC8MHU3vGhClSBj_B" data-callback="recaptcha_callback" data-expired-callback="recaptcha_expired" id="captcha"></div>

                                   <div class="col-lg-4">
                                       
                                   </div>
                               

                            </div>

                            
                             <?php
       if(isset($_GET["fallo"]) && $_GET["fallo"] == 'true')
       {
          echo "<div class='form-group-inner col-lg-12' style='color:red'>Usuario o contraseña incorrecto </div>";
       }
     ?>                       
                                  

                           


                            
                            

                            

                            <div class="row">
                                <div class="col-lg-4">

                                </div>
                                <div class="col-lg-7">
                                    <div class="login-button-pro">
                                        
                                        <button id="enviar" type="submit" class="login-button login-button-lg">Iniciar sesión</button>
                                       <!--<input type="submit" name="enviar" placeholder="ENVIAR" >-->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-lg-2"></div>
            </div>
        </div>
    </div>
    <!-- login End-->

      <div class="container" style="align-content: center;">
    <div class="row">
        <div class="col-lg-12">
        <center> 
          

           <span style="font-family: 'Panton-Bold';color: #666;font-size: 16px;">Gobierno del Estado de Yucatán 2018-2024</span>
        </center>
        </div>
    </div>
      
  </div>

  <div class="container" style="align-content: center;">
    <div class="row">
        <div class="col-lg-12">
        <center>


           <span style="font-family: 'Panton-Regular';color: #999; font-size: 14px;">Instituto del Deporte del Estado de 
            Yucatán, Calle 60 n.132 x 21 Alcalá Martín<br>
            Mérida, Yucatán <br>
            Tel. 942 00 50


            </span>
          
        </center>
        </div>
    </div>
      
  </div>

   













<script type="text/javascript">

    $(document).ready(function () {
        $('#email1').focus();
        $("#signupForm").validate({
            rules: {               
                password: {
                    required: true,
                    minlength: 5
                },               
                email: {
                    required: true,
                    email: true
                },
                agree1: "required"
            },
            messages: {
                
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },               
                email: "Please enter a valid email address",
                agree1: "Please accept our policy"
            },
            errorElement: "small",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("form-text text-muted");

                // Add `has-feedback` class to the parent div.form-group
                // in order to add icons to inputs
                element.parents(".form-group").find('input').addClass("is-invalid");

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }

                // Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!element.next("span")[0]) {
                    $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
                }
            },
            success: function (label, element) {
                // Add the span element, if doesn't exists, and apply the icon classes to it.
                if (!$(element).next("span")[0]) {
                    $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
                }
            },
            highlight: function (element, errorClass, validClass) {
                $(element).parents(".form-group").addClass("is-invalid").removeClass("is-valid");
                //$(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".form-group").find('input').addClass("is-valid").removeClass("is-invalid");
                //$(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
            }
        });
    });

</script>  

 
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript">
function recaptcha_callback() {
    $('#enviar').prop('disabled', false);
}
function recaptcha_expired() {
    $('#enviar').prop('disabled', true);
}

</script>





</body>
</html>