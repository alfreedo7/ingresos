<?php
session_start();
include_once('../../clases/seguridad.php');
$seg=new seguridad();
$seg->candado("../../login.php");
$seg->candado_permiso($_SESSION['idUsuario'], 'ADMIN');

if(isset($_POST['logOut'])){
    $seg->cerrar_sesion("../../login.php");
}


include_once('../../clases/usuario.php');
$ObjUser=new usuario();
$ObjUser->idUsuario=$_SESSION['idUsuario'];
$ObjUser->obtener_usuario();


include_once('../../clases/alumno.php');

$unidad=new unidad();
$lista_unidad=$unidad->listar_unidad();
$estatus=new estatus();
$lista_estatus=$estatus->listar_estatus();

$alumno=new alumno();
if(isset($_GET['id']))
{
    $operacion='Modificar';
    $alumno->idAlumno=$_GET['id'];
    $alumno->obtener_alumno();
    
    $boton_eliminar='<input type="submit"  name="operacion" class="btn btn-primary" value="Eliminar" >';
}else{
    $operacion='Agregar';
    $boton_eliminar='';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="shortcut icon" type="image/x-icon" href="../../img/idey.ico">

  <title>Editar Alumno || Ingresos</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php include_once ('../elementos/sidebar-admin.php'); ?>
    

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->

        <?php include_once ('../elementos/topbar-admin.php');  ?>
        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
           

           

           

          <!-- Content Row -->
          <div class="row">

            

            <div class="col-lg-12 mb-4">

              <!-- Illustrations -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Editar Alumno</h6>
                </div>
                <div class="card-body">
                  <div class="text-center">

              <form action="operaciones_alumno.php" method="POST">

              <input type="hidden" value="<?php echo $alumno->idAlumno;?>" name="idAlumno" class="form-control">
                    <div class="form-row">
                      <div class="form-group col-md-5">
                        <label for="inputEmail4">Nombre del Alumno</label>
                        <input type="text" value="<?php echo $alumno->nomalumno;?>" name="nomalumno" class="form-control" required /> 
                      </div>
                      <div class="form-group col-md-2">
                        <label for="inputPassword4">Edad</label>
                        <input type="text" value="<?php echo $alumno->edad;?>" name="edad" class="form-control" required />  
                      </div>

                      <div class="form-group col-md-5">
                         
                        <label for="inputEmail4">Dirección</label>
                        <input type="text" value="<?php echo $alumno->direccion;?>" name="direccion" class="form-control" required="" />
                      </div>
                    </div>

                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="inputEmail4">Teléfono</label>
                        <input type="number" value="<?php echo $alumno->telefono;?>" name="telefono" class="form-control" required /> 
                      </div>

                      <div class="form-group col-md-3">
                        <label for="inputPassword4">Fecha de Nacimiento</label>
                        <input type="date" value="<?php echo $alumno->fecha_nac;?>" name="fecha_nac" class="form-control" required />  
                      </div>

                      <div class="form-group col-md-5">
                         
                        <label for="inputEmail4">Lugar de Nacimiento</label>
                        <input type="text" value="<?php echo $alumno->lugar_nac;?>" name="lugar_nac" class="form-control" required="" />
                      </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                          <label for="inputPassword4">Género*</label>
                          <div class="form-select-list">
                          <select class="form-control custom-select-value" name="genero" required>
                          <option value="<?php echo $alumno->genero;?>"><?php echo $alumno->genero;?></option>
                                                      <option value="H">H</option>
                                                      <option value="M">M</option>
                          </select>
                        </div>  
                        </div>

                      <div class="form-group col-md-3">
                        <label for="inputPassword4">Tipo de Sangre</label>
                        <input type="text" value="<?php echo $alumno->tipo_sangre;?>" name="tipo_sangre" class="form-control" required />  
                      </div>
                  
                      <div class="form-group col-md-3">
                        <label for="inputPassword4">Unidad Deportiva</label>
                        <div class="form-select-list">
                                                                <select class="form-control custom-select-value" name="idUnidad">
                                                                    <?php 
                                      foreach ($lista_unidad as $elemento) {
                                        $select='';
                                        if ($elemento['idUnidad']==$alumno->idUnidad) 
                                          $select=' selected';
                                        echo '<option '.$select.' value="'.$elemento['idUnidad'].'">'.$elemento['nombre_unidad'].'</option>';
                                      }
                                  ?>
                                                                    
                                                                </select>
                                                            </div>
                      </div>
                      <div class="form-group col-md-3">
                        <label for="inputPassword4">Estatus</label>
                        <div class="form-select-list">
                                                                <select class="form-control custom-select-value" name="idEstatus">
                                                                    <?php 
                                      foreach ($lista_estatus as $elemento) {
                                        $select='';
                                        if ($elemento['idEstatus']==$alumno->idEstatus) 
                                          $select=' selected';
                                        echo '<option '.$select.' value="'.$elemento['idEstatus'].'">'.$elemento['nombre_estatus'].'</option>';
                                      }
                                  ?>
                                                                    
                                                                </select>
                                                            </div>
                      </div>
                    </div>
                    
                   <br> 

                    <div class="form-row">
                      <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-outline-primary" name="operacion" onclick="return confirmar()" value="<?php echo "$operacion";?>">Editar</button>
                        <?php
                                                                      echo $boton_eliminar;
                                                                    ?>
                                                                 
                      </div>
                      <div class="form-group col-md-1">
                         <button type="submit" class="btn btn-outline-secondary" name="operacion" onclick="return confirmar()" value="<?php echo "$operacion";?>">Cancelar</button>  
                      </div>
                      <div class="form-group col-md-9">

                        
                      </div>
                    </div>
              </form>

                  </div> 
                   
                </div>
              </div>

              

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->

      <?php include_once ('../elementos/footer-admin.php');   ?>
     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../../vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../js/demo/chart-area-demo.js"></script>
  <script src="../../js/demo/chart-pie-demo.js"></script>



</body>

</html>
