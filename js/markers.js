// Initialize and add the map
function initMap() {
  // La ubicación de Uluru
  var panaba = {lat: 21.2968826, lng: -88.2711839};
  
  var sinanche = {lat: 21.225562, lng: -89.185715};

  var tzucacab = {lat: 20.071845, lng: -89.049507};

  var buctzotz = {lat: 21.2017854, lng: -88.794531};

  var cantamayec = {lat: 20.470536, lng: -89.080918};

  var celestun = {lat: 20.860197, lng: -90.39945};


  //Texto Panaba

   var texto1 =  '<div class="container">' +
                '<div class="row">' +
                  '<div class="col-sm"><h3>PANABÁ</h3></div>'+     
                '</div>'+
                '<div class="row">' +
                  '<div class="col-sm-4">' +
                     '<img src="img/presidente-panaba.png">' +
                  '</div>' +
                  '<div class="col-sm-8 texto">Presidente: BR. Jorge Abraham Jiménez Iuit<br><br>Partido: <img src="img/pan.png" width="26px;"><br><br>Poblacion: 7,461<br><br>Cabecera: Panabá<br><br>Niños: 32</div>' +                  
                '</div>' +

                      '<hr size="10px" color="black"/>' +
                  '<div class="row">'+
                  '<div class="col-sm texto"> Director Deportivo: Andy Albert Sandoval Chan<br>Instructor: Jorge Carlos Couoh Perez<br>'+
                  '</div>'+                  
                '</div>'+ 
                '<hr size="10px" color="black"/>' +
                '<table class="table">'+
										    ' <thead>' +
										      ' <tr>' +
										       '  <th>Disciplina</th>' +
										       '  <th>Campo</th>' +
										       '  <th>Días</th>' +
										       '  <th>Hora</th>' +
										       '  <th>Niños</th>' +
										      ' </tr>' +
										    ' </thead>' +
										    ' <tbody>' +
										       
										     '  <tr>' +
										       '  <td>Beísbol</td>' +
										       '  <td>De softbol</td>' +
										       '  <td>Lunes y martes</td>' +
										       '  <td>15:00-17:00</td>' +
										       '  <td>12</td>' +
										      ' </tr>' +
										       '  <tr>' +
										       '  <td>Fútbol</td>' +
										       '  <td>Delos Tigres</td>' +
										       '  <td>Jueves y viernes</td>' +
										       '  <td>15:00-17:00</td>' +
										       '  <td>20</td>' +
										      ' </tr>' +
										   '  </tbody>' +
										  ' </table>'+

                '<div class="row disciplina">' +
                 '<div class="col-sm-6">Campo Beisbol</div>' +
                 '<div class="col-sm-6">Campo Futbol</div>' +       
               '</div>' +

               '<div class="row foto">' +
                  '<div class="col-sm-6"><img src="img/beisbol-panaba.png"></div>' +
                  '<div class="col-sm-6"><img src="img/futbol-panaba.png"></div>' +      
                '</div>' +
                  
                 
        
              '</div>';


 //Texto sinanche

   var texto2 =  '<div class="container">' +
                '<div class="row">' +
                  '<div class="col-sm"><h3>SINANCHÉ</h3></div>'+     
                '</div>'+
                '<div class="row">' +
                  '<div class="col-sm-4">' +
                     '<img src="img/presidente-sinanche.jpg">' +
                  '</div>' +
                  '<div class="col-sm-8 texto">Presidente: Dr. Felipe Rojas Escalante<br><br>Partido: <img src="img/pan.png" width="26px;"><br><br>Poblacion: 3,126<br><br>Cabecera: Sinanché <br><br>Niños: 58</div>' +                  
                '</div>' +

                      '<hr size="10px" color="black"/>' +
                  '<div class="row">'+
                  '<div class="col-sm texto"> Director Deportivo: Durbin Parra Gurubel<br>Instructor(es): Reyes Quijano y Jose Sandoval<br>'+
                  '</div>'+                  
                '</div>'+ 
                '<hr size="10px" color="black"/>' +
                '<table class="table">'+
										    ' <thead>' +
										      ' <tr>' +
										       '  <th>Disciplina</th>' +
										       '  <th>Campo</th>' +
										       '  <th>Días</th>' +
										       '  <th>Hora</th>' +
										       '  <th>Niños</th>' +
										      ' </tr>' +
										    ' </thead>' +
										    ' <tbody>' +
										       
										     '  <tr>' +
										       '  <td>Beísbol</td>' +
										       '  <td>Felipe Carrillo Puerto</td>' +
										       '  <td>Lunes a viernes</td>' +
										       '  <td>16:00-18:00</td>' +
										       '  <td>23</td>' +
										      ' </tr>' +
										       '  <tr>' +
										       '  <td>Fútbol</td>' +
										       '  <td>Atrio de la iglesia</td>' +
										       '  <td>Martes, mier y viernes</td>' +
										       '  <td>16:00-18:00</td>' +
										       '  <td>35</td>' +
										      ' </tr>' +
										   '  </tbody>' +
										  ' </table>'+

                '<div class="row disciplina">' +
                 '<div class="col-sm-6">Campo Beisbol</div>' +
                 '<div class="col-sm-6">Campo Futbol</div>' +       
               '</div>' +

               '<div class="row foto">' +
                  '<div class="col-sm-6"><img src="img/beisbol-sinanche.jpg"></div>' +
                  '<div class="col-sm-6"><img src="img/futbol-sinanche.jpg"></div>' +      
                '</div>' +
                  
                 
        
              '</div>';


              //Texto tzucacab

   var texto3 =  '<div class="container">' +
                '<div class="row">' +
                  '<div class="col-sm"><h3>TZUCACAB</h3></div>'+     
                '</div>'+
                '<div class="row">' +
                  '<div class="col-sm-4">' +
                     '<img src="img/presidente-tzucacab.png">' +
                  '</div>' +
                  '<div class="col-sm-8 texto">Presidente: C. Javier Cuy Canul<br><br>Partido: <img src="img/prd.png" width="26px;"><br><br>Poblacion: 14,011<br><br>Cabecera: Tzucacab<br><br> Niños: 20</div>' +                  
                '</div>' +

                      '<hr size="10px" color="black"/>' +
                  '<div class="row">'+
                  '<div class="col-sm texto"> Director Deportivo: Jorge Pineda Estrella<br>Instructor: Carlos Pech Vázquez<br>'+
                  '</div>'+                  
                '</div>'+ 
                '<hr size="10px" color="black"/>' +
                '<table class="table">'+
										    ' <thead>' +
										      ' <tr>' +
										       '  <th>Disciplina</th>' +
										       '  <th>Campo</th>' +
										       '  <th>Días</th>' +
										       '  <th>Hora</th>' +
										       '  <th>Niños</th>' +
										      ' </tr>' +
										    ' </thead>' +
										    ' <tbody>' +
										       
										     '  <tr>' +
										       '  <td>Beísbol</td>' +
										       '  <td>Manuel el chueco Araujo</td>' +
										       '  <td>Martes y jueves</td>' +
										       '  <td>15:30-18:30</td>' +
										       '  <td>14</td>' +
										      ' </tr>' +
										       '  <tr>' +
										       '  <td>Fútbol</td>' +
										       '  <td>De la placita</td>' +
										       '  <td>Lunes, mier y viernes</td>' +
										       '  <td>17:00-19:00</td>' +
										       '  <td>32</td>' +
										      ' </tr>' +
										   '  </tbody>' +
										  ' </table>'+

                '<div class="row disciplina">' +
                 '<div class="col-sm-6"> </div>' +
                 '<div class="col-sm-6">Campo Futbol</div>' +       
               '</div>' +

               '<div class="row foto">' +
                  '<div class="col-sm-6"> <div>' +
                  '<div class="col-sm-6"><img src="img/futbol-tzucacab.png"></div>' +      
                '</div>' +
                  
                 
        
              '</div>';


              //Texto buctzotz

   var texto4 =  '<div class="container">' +
                '<div class="row">' +
                  '<div class="col-sm"><h3>BUCTZOTZ</h3></div>'+     
                '</div>'+
                '<div class="row">' +
                  '<div class="col-sm-4">' +
                     '<img src="img/presidente-buctzotz.png">' +
                  '</div>' +
                  '<div class="col-sm-8 texto">Presidente: PROFA. Margarita Sánchez Medrano<br><br>Partido: <img src="img/pri.png" width="26px;"><br><br>Poblacion: 8,637<br><br>Cabecera: Tizimín<br><br>Niños: 48</div>' +                  
                '</div>' +

                      '<hr size="10px" color="black"/>' +
                  '<div class="row">'+
                  '<div class="col-sm texto"> Director Deportivo: Eloy Medrano Rivero<br>Instructor: Miguel Torres Canul<br>'+
                  '</div>'+                  
                '</div>'+ 
                '<hr size="10px" color="black"/>' +
                '<table class="table">'+
                        ' <thead>' +
                          ' <tr>' +
                           '  <th>Disciplina</th>' +
                           '  <th>Campo</th>' +
                           '  <th>Días</th>' +
                           '  <th>Hora</th>' +
                           '  <th>Niños</th>' +
                          ' </tr>' +
                        ' </thead>' +
                        ' <tbody>' +
                           
                         '  <tr>' +
                           '  <td>Beísbol</td>' +
                           '  <td>De la entrada</td>' +
                           '  <td>Martes, jueves y viernes</td>' +
                           '  <td>15:00-17:00</td>' +
                           '  <td>24</td>' +
                          ' </tr>' +
                           '  <tr>' +
                           '  <td>Fútbol</td>' +
                           '  <td>Cancha del centro</td>' +
                           '  <td>Martes, jueves y viernes</td>' +
                           '  <td>17:00-19:00</td>' +
                           '  <td>24</td>' +
                          ' </tr>' +
                       '  </tbody>' +
                      ' </table>'+

                '<div class="row disciplina">' +
                 '<div class="col-sm-6">Campo Beisbol</div>' +
                 '<div class="col-sm-6">Campo Futbol</div>' +       
               '</div>' +

               '<div class="row foto">' +
                  '<div class="col-sm-6"> </div>' +
                  '<div class="col-sm-6"> </div>' +      
                '</div>' +
                  
                 
        
              '</div>';



              //Texto cantamayec

   var texto5 =  '<div class="container">' +
                '<div class="row">' +
                  '<div class="col-sm"><h3>CANTAMAYEC</h3></div>'+     
                '</div>'+
                '<div class="row">' +
                  '<div class="col-sm-4">' +
                     '<img src="img/presidente-cantamayec.png">' +
                  '</div>' +
                  '<div class="col-sm-8 texto">Presidente: Lic.Dorca Grisselda Chan May<br><br>Partido: <img src="img/pan.png" width="26px;"><br><br>Poblacion: 2,407<br><br>Cabecera: Valladolid<br><br>Niños: 8</div>' +                  
                '</div>' +

                      '<hr size="10px" color="black"/>' +
                  '<div class="row">'+
                  '<div class="col-sm texto"> Director Deportivo: Marcelino Ku<br>Instructor(es): Gilberto Dominguez y Eric Canché<br>'+
                  '</div>'+                  
                '</div>'+ 
                '<hr size="10px" color="black"/>' +
                '<table class="table">'+
                        ' <thead>' +
                          ' <tr>' +
                           '  <th>Disciplina</th>' +
                           '  <th>Campo</th>' +
                           '  <th>Días</th>' +
                           '  <th>Hora</th>' +
                           '  <th>Niños</th>' +
                          ' </tr>' +
                        ' </thead>' +
                        ' <tbody>' +
                           
                         '  <tr>' +
                           '  <td>Beísbol</td>' +
                           '  <td></td>' +
                           '  <td>Lunes, mier y jueves</td>' +
                           '  <td></td>' +
                           '  <td></td>' +
                          ' </tr>' +
                           '  <tr>' +
                           '  <td>Fútbol</td>' +
                           '  <td>Municipal</td>' +
                           '  <td>Martes, jueves y viernes</td>' +
                           '  <td>15:30-17:30</td>' +
                           '  <td>8</td>' +
                          ' </tr>' +
                       '  </tbody>' +
                      ' </table>'+

                '<div class="row disciplina">' +
                 '<div class="col-sm-6">Campo Beisbol</div>' +
                 '<div class="col-sm-6">Campo Futbol</div>' +       
               '</div>' +

               '<div class="row foto">' +
                  '<div class="col-sm-6"> </div>' +
                  '<div class="col-sm-6"><img src="img/futbol-cantamayec.png"></div>' +      
                '</div>' +
                  
                 
        
              '</div>';



              //Texto celestun

   var texto6 =  '<div class="container">' +
                '<div class="row">' +
                  '<div class="col-sm"><h3>CELESTÚN</h3></div>'+     
                '</div>'+
                '<div class="row">' +
                  '<div class="col-sm-4">' +
                     '<img src="img/presidente-celestún.png">' +
                  '</div>' +
                  '<div class="col-sm-8 texto">Presidente: C.Yulma Yumira García Casanova<br><br>Partido: <img src="img/pri.png" width="26px;"><br><br>Poblacion: 6,831<br><br>Cabecera: Maxcanú<br><br>Niños: 40</div>' +                  
                '</div>' +

                      '<hr size="10px" color="black"/>' +
                  '<div class="row">'+
                  '<div class="col-sm texto"> Director Deportivo: Hugo Suárez<br>Instructor(es): Orlando Chan Mena y José Poot Yerves<br>'+
                  '</div>'+                  
                '</div>'+ 
                '<hr size="10px" color="black"/>' +
                '<table class="table">'+
                        ' <thead>' +
                          ' <tr>' +
                           '  <th>Disciplina</th>' +
                           '  <th>Campo</th>' +
                           '  <th>Días</th>' +
                           '  <th>Hora</th>' +
                           '  <th>Niños</th>' +
                          ' </tr>' +
                        ' </thead>' +
                        ' <tbody>' +
                           
                         '  <tr>' +
                           '  <td>Beísbol</td>' +
                           '  <td>De Béisbol Celestún</td>' +
                           '  <td>Lunes, viernes y sabado</td>' +
                           '  <td>15:30-18:00 y Sab: 16:00-18:00</td>' +
                           '  <td>25</td>' +
                          ' </tr>' +
                           '  <tr>' +
                           '  <td>Fútbol</td>' +
                           '  <td>Cancha fut 7</td>' +
                           '  <td>Lunes y sabado</td>' +
                           '  <td>17:30-19:30</td>' +
                           '  <td>15</td>' +
                          ' </tr>' +
                       '  </tbody>' +
                      ' </table>'+

                '<div class="row disciplina">' +
                 '<div class="col-sm-6">Campo Beisbol</div>' +
                 '<div class="col-sm-6">Campo Futbol</div>' +       
               '</div>' +

               '<div class="row foto">' +
                  '<div class="col-sm-6"> </div>' +
                  '<div class="col-sm-6"> </div>' +      
                '</div>' +
                  
                 
        
              '</div>';



//****************************** MARCADORES ************************//


// El mapa, centrado en tzucacab
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 8, center: tzucacab});


 // El marcador, colocado en panaba
  var marker = new google.maps.Marker({
    position: panaba,
     map: map,
     title : "Panabá"
   });


  // El marcador, colocado en sinanche
  var marker2 = new google.maps.Marker({
    position: sinanche,
     map: map,
     title : "Sinanché"
   });


  // El marcador, colocado en tzucacab
  var marker3 = new google.maps.Marker({
    position: tzucacab,
     map: map,
     title : "Tzucacab"
   });

   // El marcador, colocado en buctzotz
  var marker4 = new google.maps.Marker({
    position: buctzotz,
     map: map,
     title : "Buctzotz"
   });

    // El marcador, colocado en cantamayec
  var marker5 = new google.maps.Marker({
    position: cantamayec,
     map: map,
     title : "Cantamayec"
   });

  // El marcador, colocado en celestún
  var marker6 = new google.maps.Marker({
    position: celestun,
     map: map,
     title : "Celestún"
   });


//************************  TEXTOS  ***************************************************//
   


  //Texto del panaba
  var informacion = new google.maps.InfoWindow({
    content: texto1
     
  });

  //Texto del sinanche

  var informacion2 = new google.maps.InfoWindow({
    content: texto2
     
  });tzucacab

  //Texto del tzucacab

  var informacion3 = new google.maps.InfoWindow({
    content: texto3
     
  });


   //Texto de buctzotz

  var informacion4 = new google.maps.InfoWindow({
    content: texto4
     
  });

   //Texto de cantamayec

  var informacion5 = new google.maps.InfoWindow({
    content: texto5
     
  });

  //Texto de celestún

  var informacion6 = new google.maps.InfoWindow({
    content: texto6
     
  });



//********************* FUNCION CLIC *************************************//   


//funcion clic del panaba
  marker.addListener('click', function(){
    informacion.open(map,marker)

  });

//funcion clic del sinanche
  marker2.addListener('click', function(){
    informacion2.open(map,marker2)

  });

  //funcion clic del tzucacab
  marker3.addListener('click', function(){
    informacion3.open(map,marker3)

  });

   //funcion clic del buctzotz
  marker4.addListener('click', function(){
    informacion4.open(map,marker4)

  });

   //funcion clic cantamayec
  marker5.addListener('click', function(){
    informacion5.open(map,marker5)

  });

  //funcion clic celestún
  marker6.addListener('click', function(){
    informacion6.open(map,marker6)

  });




  



}
   